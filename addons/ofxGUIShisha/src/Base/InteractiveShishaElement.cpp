/*
 *  InteractiveShishaElement.cpp
 *  openFrameworks
 *
 *  Created by Pat Long on 01/04/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */

#include "InteractiveShishaElement.h"

void InteractiveShishaElement::init(int x, int y, int width, int height){
	ShishaElement::init(x, y, width, height);
	this->setName("Interactive Shisha Element");
	this->elementType = SHISHA_TYPE_INTERACTIVE;
	this->setDragMode(DEFAULT_DRAG_MODE);
	this->setElasticSnap(DEFAULT_ELASTIC_SNAP);
	this->originalX = x;
	this->originalY = y;
	this->moveToX = this->moveToY = -1;
	this->lastX = this->lastY = -1;
	this->isMoving = false;
}

bool InteractiveShishaElement::prepareForMotion(){
	this->snapBack(0);
	return true;
}

void InteractiveShishaElement::setDragMode(int dragMode, int elasticSnap, bool allowDragX, bool allowDragY){
	this->dragMode = dragMode;
	if(dragMode == SHISHA_DRAG_MODE_NONE){
		this->allowDragX = this->allowDragY = false;
	}
	else{
		this->allowDragX = allowDragX;
		this->allowDragY = allowDragY;
		if(dragMode == SHISHA_DRAG_MODE_ELASTIC){
			this->setElasticSnap(elasticSnap);
		}
	}
}

void InteractiveShishaElement::setElasticSnap(int elasticSnap){
	this->elasticSnap = elasticSnap;
}

void InteractiveShishaElement::setPosition(float x, float y){
	ShishaElement::setPosition(x, y);
	this->originalX = x;
	this->originalY = y;
}

void InteractiveShishaElement::updatePosition(int x, int y){
	this->lastX = this->x;
	this->lastY = this->y;
	this->x = x;
	this->y = y;
	this->onMove(this->x-this->lastX, this->y-this->lastY); // trigger the event
	this->onPosition(x, y, this->lastX, this->lastY);
}



/**void InteractiveShishaElement::draw(){
	ShishaElement::draw();
}

void InteractiveShishaElement::draw(float x, float y, float w, float h, bool borders){
	ShishaElement::draw(x, y, w, h, borders);
}
*/
void InteractiveShishaElement::update(){
	ShishaElement::update();
	this->snapCheck();
	this->isMoving = this->move();
}

bool InteractiveShishaElement::snapCheck(){
	if(this->dragMode == SHISHA_DRAG_MODE_NONE){
		this->snapBack(0);
		return true;
	}
	else if(this->dragMode == SHISHA_DRAG_MODE_ELASTIC){
		if(this->x != this->originalX || this->y != this->originalY){
			this->snapBack(this->elasticSnap);
			return true;
		}
	}
	return false;
}

void InteractiveShishaElement::snapTo(float x, float y, int snapTime){
	if(snapTime <= 0){
		this->updatePosition(x, y);
		this->endMove();
	}
	else{
		this->moveToX = x;
		this->moveToY = y;
		this->dx = (x - this->x) / (float)snapTime;
		this->dy = (y - this->y) / (float)snapTime;
	}
}

void InteractiveShishaElement::snapBack(int snapTime){
	this->snapTo(this->originalX, this->originalY, snapTime);
}

bool InteractiveShishaElement::move(){
	if(this->dx != 0.0 || this->dy != 0.0){
		if((this->dx == 0.0 || this->dx < 0.0 && this->x <= this->moveToX || this->dx > 0.0 && this->x >= this->moveToX)
		   && (this->dy == 0.0 || this->dy < 0.0 && this->y <= this->moveToY || this->dy > 0.0 && this->y >= this->moveToY)){
			this->endMove();
			return false;
		}
		this->updatePosition(this->x+this->dx, this->y+this->dy);
		return true;
	}
}

void InteractiveShishaElement::endMove(){
	this->moveToX = this->moveToY = -1.0;
	this->lastX = this->lastY = -1.0;
	this->dx = this->dy = 0.0;
}
/**
void InteractiveShishaElement::grabMoveBackground(float x, float y, float w, float h){
	if(x == -1.0)
		x = this->x;
	if(y == -1.0)
		y = this->y;
	if(w == -1.0)
		w = this->width;
	if(h == -1.0)
		h = this->height;
	this->moveBackground.grabScreen(x, y, w, h);
}*/

bool InteractiveShishaElement::checkCursorDrag(int x, int y, int button, string cursorID, float w, float h){
	bool check = ShishaElement::checkCursorDrag(x, y, button, cursorID, w, h);
	if(check){
	   // it's hovered, handle the dragging
	   if(this->dragMode != SHISHA_DRAG_MODE_NONE){
		   float xDrag = 0;
		   float yDrag = 0;
		   if(this->allowDragX)
			   xDrag = this->cursors.getMoveX(cursorID);
		   if(this->allowDragY)
			   yDrag = this->cursors.getMoveY(cursorID);
		   this->updatePosition(this->x+xDrag, this->y+yDrag);
	   }
		return check;
	}
	return check;
}
/**
bool InteractiveShishaElement::checkCursorPress(int x, int y, int button, string cursorID, float w, float h){
}*/
