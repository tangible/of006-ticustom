/*
 *  BasicButton.cpp
 *  openFrameworks
 *
 *  Created by Pat Long on 23/04/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */

#include "BasicButton.h"

BasicButton::BasicButton(){
}

BasicButton::~BasicButton(){
}

void BasicButton::init(float x, float y, float width, float height, int elementID){
	ShishaElement::init(x, y, width, height, elementID);
	this->setHitLocation();
	this->elementType = SHISHA_TYPE_HOOKAH_BUTTON;
	this->setName("BasicButton");
	this->setLabel("Button");
	this->active = false;
	this->triggered = false;
	this->setClickMode(BUTTON_CLICK_MODE_ANY);
	this->holdDelay = DEFAULT_HOLD_DELAY;
	this->triggeredBeforeHold = false;
	this->pressTime = -1;
	this->pressTriggerDelay = DEFAULT_PRESS_TRIGGER_DELAY;
	this->lastPressTrigger = -1;
	this->buttonStyle = BUTTON_STYLE_LABEL_OVERLAY;
	this->drawHitBorders = false;
#ifdef DRAW_HIT_BORDERS
	this->drawHitBorders = true;
#endif
}

void BasicButton::moveHitLocation(float moveX, float moveY){
	this->hitX += moveX;
	this->hitY += moveY;
}

void BasicButton::resizeHitLocation(float xScale, float yScale){
	this->hitWidth *= xScale;
	this->hitHeight *= yScale;
}

bool BasicButton::isHovered(string cursorID, float x, float y){
	int tX = x;
	int tY = y;
	if(this->parent != NULL)
		this->parent->translateMouseCoords(tX, tY);
	return (tX >= this->hitX && tX <= this->hitX+this->hitWidth && tY >= this->hitY && tY <= this->hitY+this->hitHeight);
}

void BasicButton::onDrag(string cursorID){
	ShishaElement::onDrag(cursorID);
	this->onPress(cursorID);
}

void BasicButton::onDragOn(string cursorID){
	ShishaElement::onDragOn(cursorID);
	this->onPress(cursorID);
}

void BasicButton::onDragOff(string cursorID){
	ShishaElement::onDragOff(cursorID);
	this->onRelease(cursorID);
}

void BasicButton::onPress(string cursorID){
	ShishaElement::onPress(cursorID);
	this->active = true;
	int cTime = ofGetElapsedTimeMillis();
	if(this->pressTime == -1){
		this->triggered = true;
		this->triggeredBeforeHold = false;
		this->lastPressTrigger = cTime;
		this->pressTime = cTime;
	}
}

void BasicButton::onRelease(string cursorID){
//	if(cursorID == MOUSE_ID || this->clickMode == BUTTON_CLICK_MODE_ANY || (this->clickMode == BUTTON_CLICK_MODE_SINGLE && (!this->cursors.isClickedOn(cursorID) || !this->cursors.isReleasedOn(cursorID))))
		ShishaElement::onRelease(cursorID);
	this->active = false;
	this->pressTime = -1;
	this->triggered = false;
	this->triggeredBeforeHold = false;
	this->lastPressTrigger = -1;
}

void BasicButton::onMove(float moveX, float moveY){
	this->moveHitLocation(moveX, moveY);
}

void BasicButton::onScale(float xScale, float yScale){
	this->resizeHitLocation(xScale, yScale);
}

void BasicButton::update(){
	if(this->isActive()){
		int cTime = ofGetElapsedTimeMillis();
		this->triggered = false;
		// press time is when it was first activated
		// lastPressTrigger is last time it was triggered
		if(this->holdDelay <= 0 || (this->pressTime != -1 && (cTime-this->pressTime) >= this->holdDelay)){
			if(this->pressTriggerDelay <= 0 || this->lastPressTrigger == -1 || (cTime-this->lastPressTrigger) >= this->pressTriggerDelay){
				this->lastPressTrigger = cTime;
				this->triggered = true;
			}
		}
		else if(!this->triggeredBeforeHold){ // && this->pressTime != -1 && (cTime-this->pressTime) < this->holdDelay){
			this->triggered = true;
			this->triggeredBeforeHold = true;
		}
	}
}

void BasicButton::draw(float x, float y, float w, float h){
	ShishaElement::draw(x, y, w, h, this->getTheme()->drawButtonBorders());
	
	if(this->drawHitBorders){
		ofNoFill();
		ofSetColor(255, 0, 0);
		ofRect(this->hitX, this->hitY, this->hitWidth, this->hitHeight);
		this->getTheme()->setColour();
		ofFill();
	}
	
	if(this->getLabel() != ""){
		this->getTheme()->setColour(SHISHA_COLOUR_FOREGROUND_1, this);
		if(this->buttonStyle == BUTTON_STYLE_LABEL_LEFT)
			this->getTheme()->drawText(this->getLabel(), x, y+h/2.0, false);
		else if(this->buttonStyle == BUTTON_STYLE_LABEL_RIGHT)
			this->getTheme()->drawText(this->getLabel(), x+h+5, y+h/2.0, false);
		else // OVERLAY
			this->getTheme()->drawText(this->getLabel(), x+w/2.0, y+h/2.0, true);
	}
	this->getTheme()->setColour();
}

void BasicButton::drawAsRectangle(float x, float y, float w, float h, bool borders){	
	if(this->buttonStyle == BUTTON_STYLE_LABEL_OVERLAY){
		ShishaElement::drawAsRectangle(x, y, w, h, borders);
		if(this->isActive() && this->elementType == SHISHA_TYPE_HOOKAH_BUTTON){
			this->getTheme()->setColour(SHISHA_COLOUR_FOREGROUND_2, this);
			ofRect(x+1, y+1, w-1, h-1);
		}
	}
	else{
		float xOffset = 0;
		float yOffset = 0;
		if(this->buttonStyle == BUTTON_STYLE_LABEL_LEFT)
			xOffset = w - h;

		this->getTheme()->setColour(SHISHA_COLOUR_BACKGROUND, this);
		ofFill();
		ofRect(x+xOffset, y+yOffset, h, h);
		
		if(borders){
			this->getTheme()->setColour(SHISHA_COLOUR_BORDER, this);	
			ofNoFill();
			ofRect(x+xOffset, y+yOffset, h, h);
			ofFill();
		}
	}
}

void BasicButton::drawAsCircle(float x, float y, float w, float h, bool borders){
	if(this->buttonStyle == BUTTON_STYLE_LABEL_OVERLAY){
		ShishaElement::drawAsCircle(x, y, w, h, borders);
		if(this->isActive() && this->elementType == SHISHA_TYPE_HOOKAH_BUTTON){
			this->getTheme()->setColour(SHISHA_COLOUR_FOREGROUND_2, this);
			float radius = (w-2)/2.0;
			ofCircle(x+1+radius, y+1+radius, radius);
		}
	}
	else{
		float radius = h/2.0;
		int xOffset = 0;
		int yOffset = 0;
		if(this->buttonStyle == BUTTON_STYLE_LABEL_LEFT)
			xOffset = w - radius*2.0;
		
		this->getTheme()->setColour(SHISHA_COLOUR_BACKGROUND, this);
		ofFill();
		ofCircle(x+xOffset+radius, y+yOffset+radius, radius);
		
		if(borders){
			this->getTheme()->setColour(SHISHA_COLOUR_BORDER, this);	
			ofNoFill();
			ofCircle(x+xOffset+radius, y+yOffset+radius, radius);
			ofFill();
		}
	}
}

void BasicButton::setClickMode(int clickMode){
	this->clickMode = clickMode;
}

void BasicButton::setHitLocation(float x, float y, float width, float height){
	if(x == -1.0)
		x = this->x;
	if(y == -1.0)
		y = this->y;
	if(width == -1.0)
		width = this->width;
	if(height == -1.0)
		height = this->height;
	this->hitX = x;
	this->hitY = y;
	this->hitWidth = width;
	this->hitHeight = height;
}

void BasicButton::setButtonStyle(int buttonStyle){
	this->buttonStyle = buttonStyle;
}

int BasicButton::getGuiDisplayState(){
	if(this->isActive())
		return(this->hasCursors()?GUI_ELEMENT_DISPLAY_STATE_ACTIVE_HOVER:GUI_ELEMENT_DISPLAY_STATE_ACTIVE);
	else
		return(this->hasCursors()?GUI_ELEMENT_DISPLAY_STATE_INACTIVE_HOVER:GUI_ELEMENT_DISPLAY_STATE_INACTIVE);
}

bool BasicButton::isActive(){
	return this->active;
}

bool BasicButton::isTriggered(){
	return this->triggered;
}

bool BasicButton::checkActive(){
	bool check = this->active;
	this->active = false;
	return check;
}

bool BasicButton::wasClickedAndReleased(bool resetCursorIDs){
	string clickAndReleaseID = this->cursors.getClickedAndReleased();
	if(clickAndReleaseID != "" && resetCursorIDs){
//		cout << this->getName() << ":clickAndRelease:" << clickAndReleaseID << ":" << endl;
		this->unsetCursor(clickAndReleaseID);
	}
	return (clickAndReleaseID != "");
}

bool BasicButton::getBool(string selector){
	if(this->selectSelf(selector))
		return this->isTriggered();
	return false;
}

void BasicButton::setBool(bool value){
	this->active = value;
	if(this->active)
		this->onPress("setBool");
	else
		this->onRelease("setBool");
}
