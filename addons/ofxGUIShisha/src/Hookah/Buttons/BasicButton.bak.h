/*
 *  BasicButton.h
 *  openFrameworks
 *
 *  Created by Pat Long on 23/04/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */
#ifndef _OFX_GUISHISHA_BASIC_BUTTON
#define _OFX_GUISHISHA_BASIC_BUTTON

#include "ShishaElement.h"

//#define DRAW_HIT_BORDERS

#define DEFAULT_DRAG_TRIGGER_DELAY 50

#define BUTTON_CLICK_MODE_ANY		0
#define BUTTON_CLICK_MODE_SINGLE	1

#define BUTTON_STYLE_LABEL_OVERLAY	0
#define BUTTON_STYLE_LABEL_RIGHT	1
#define BUTTON_STYLE_LABEL_LEFT		2

class BasicButton : public ShishaElement{
	protected:
		float hitX, hitY, hitWidth, hitHeight;
		bool active, drawHitBorders;
		int clickMode, dragTriggerDelay, lastDragTrigger;
		int buttonStyle;
	
		void moveHitLocation(float moveX, float moveY);
		void resizeHitLocation(float xScale, float yScale);
	
		virtual bool isHovered(string cursorID, float x, float y);
	
		virtual void onDrag(string cursorID);
		virtual void onPress(string cursorID);
		virtual void onRelease(string cursorID);
	
		virtual void onMove(float moveX, float moveY);
		virtual void onScale(float xScale, float yScale);
	
	public:
		BasicButton();
		~BasicButton();
		virtual void init(float x=0, float y=0, float width=DEFAULT_GUI_ELEMENT_WIDTH, float height=DEFAULT_GUI_ELEMENT_HEIGHT, int elementID=-1);
		virtual void draw(float x, float y, float w, float h);
		virtual void drawAsRectangle(float x, float y, float w, float h, bool borders);
		virtual void drawAsCircle(float x, float y, float w, float h, bool borders);

		virtual void setClickMode(int clickMode=BUTTON_CLICK_MODE_ANY);
		virtual void setHitLocation(float x=-1.0, float y=-1.0, float width=-1.0, float height=-1.0);
		virtual void setButtonStyle(int buttonStyle);
	
		virtual int getGuiDisplayState();
	
		virtual bool isActive();
		bool checkActive();
	
		bool wasClickedAndReleased(bool resetCursorIDs=true);
	
		virtual bool getBool(string selector=DEFAULT_SELECTOR);
		virtual void setBool(bool value=false);
};

#endif
