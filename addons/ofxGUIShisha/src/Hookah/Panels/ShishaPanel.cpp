/*
 *  ShishaPanel.cpp
 *  openFrameworks
 *
 *  Created by Pat Long on 23/04/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */

#include "ShishaPanel.h"

ShishaPanel::ShishaPanel(){
}

ShishaPanel::~ShishaPanel(){
}

void ShishaPanel::init(float x, float y, float width, float height, string panelLabel, string panelName){
	ShishaContainer::init(x, y, width, height);
	this->panelName = panelName;
	this->setName(panelName);
	this->setLabel(panelLabel);
	this->initTitleBar();
}

void ShishaPanel::initTitleBar(){
	this->titleBar = new ShishaPanel::TitleBar(this);
	this->titleBar->init(this->x, this->y, this->width, 20);
	ShishaContainer::addElement(this->titleBar, false, false);
	this->cOffsetY += this->titleBar->getHeight() + this->verticalSpacing;
}

void ShishaPanel::resetVerticalOffsets(){
	ShishaContainer::resetVerticalOffsets();
	this->cOffsetY += this->titleBar->getHeight();
}

ShishaPanel::TitleBar::TitleBar(ShishaPanel* panel){
	this->panel = panel;
}

ShishaPanel::TitleBar::~TitleBar(){
}

void ShishaPanel::TitleBar::init(float x, float y, float width, float height){
	ShishaElement::init(x, y, width, height);
	this->setName(this->panel->getName()+":titleBar");
	this->setLabel(this->panel->getLabel());
}

void ShishaPanel::TitleBar::draw(){
	this->draw(this->x, this->y, this->width, this->height);
}

void ShishaPanel::TitleBar::draw(float x, float y){
	this->draw(x, y, this->width, this->height);
}

void ShishaPanel::TitleBar::draw(float x, float y, float w, float h){
	ShishaElement::draw(x, y, w, h, this->getTheme()->drawPanelBorders());
	this->getTheme()->setColour(SHISHA_COLOUR_BORDER, this);
	ofLine(x, y+h, x+w, y+h);
	
	this->getTheme()->setColour(SHISHA_COLOUR_FOREGROUND_1, this);
	this->getTheme()->drawText(this->getLabel(), x+10, y+h/1.5, false);	
	this->getTheme()->setColour();
}

string ShishaPanel::getName(){
	return this->panelName;
}

void ShishaPanel::setPosition(float x, float y){
	ShishaContainer::setPosition(x, y);
//	if(this->titleBar != NULL)
//		this->titleBar->setPosition(x, y);
}

void ShishaPanel::setTheme(ShishaTheme* theme, ShishaTheme* doNotDelete, bool themeInherited){
	ShishaContainer::setTheme(theme, doNotDelete, themeInherited);
	if(this->theme != NULL)
		this->drawBorders = this->theme->drawPanelBorders();
}

void ShishaPanel::setWidth(float width){
	ShishaContainer::setWidth(width);
	this->titleBar->setWidth(width);
}
/**
void ShishaPanel::draw(){
	ShishaContainer::draw();
}

void ShishaPanel::draw(float x, float y){
	ShishaContainer::draw(x, y);
}

void ShishaPanel::draw(float x, float y, float w, float h){
	ShishaContainer::draw(x, y, w, h, this->getTheme()->drawPanelBorders());
}
*/
ShishaElement* ShishaPanel::addElement(ShishaElement* element, bool updateLocation, bool inheritTheme){
	return (ShishaElement*)ShishaContainer::addElement(element, updateLocation, inheritTheme);
}

ShishaElement* ShishaPanel::addElement(float w, float h){
	ShishaElement* newElement = new ShishaElement();
	newElement->init(this->x, this->y, w, h);
	return (ShishaElement*)ShishaContainer::addElement(newElement);
}

BasicButton* ShishaPanel::addButton(float w, float h){
	BasicButton* newElement = new BasicButton();
	newElement->init(this->x, this->y, w, h);
	return (BasicButton*)ShishaContainer::addElement(newElement);
}

ToggleButton* ShishaPanel::addToggleButton(float w, float h){
	ToggleButton* newElement = new ToggleButton();
	newElement->init(this->x, this->y, w, h);
	return (ToggleButton*)ShishaContainer::addElement(newElement);
}

RadioSet* ShishaPanel::addRadioSet(float w, float h){
	RadioSet* newElement = new RadioSet();
	newElement->init(this->x, this->y, w, h);
	return (RadioSet*)ShishaContainer::addElement(newElement);
}

Bar* ShishaPanel::addBar(float w, float h){
	Bar* newElement = new Bar();
	newElement->init(this->x, this->y, w, h);
	return (Bar*)ShishaContainer::addElement(newElement);
}

FloatBar* ShishaPanel::addFloatBar(float w, float h){
	FloatBar* newElement = new FloatBar();
	newElement->init(this->x, this->y, w, h);
	return (FloatBar*)ShishaContainer::addElement(newElement);
}

SliderBar* ShishaPanel::addSliderBar(float w, float h){
	SliderBar* newElement = new SliderBar();
	newElement->init(this->x, this->y, w, h);
	return (SliderBar*)ShishaContainer::addElement(newElement);
}

TextInput* ShishaPanel::addTextInput(float w, float h){
	TextInput* newElement = new TextInput();
	newElement->init(this->x, this->y, w, h);
	return (TextInput*)ShishaContainer::addElement(newElement);
}

TextOutput* ShishaPanel::addTextOutput(float w, float h){
	TextOutput* newElement = new TextOutput();
	newElement->init(this->x, this->y, w, h);
	return (TextOutput*)ShishaContainer::addElement(newElement);
}
