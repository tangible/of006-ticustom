/*
 *  ShishaSlidingContainer.h
 *  openFrameworks
 *
 *  Created by Pat Long on 04/06/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */
#ifndef _OFX_GUISHISHA_SHISHASLIDINGCONTAINER
#define _OFX_GUISHISHA_SHISHASLIDINGCONTAINER

#include "ShishaContainer.h"
#include "BasicButton.h"

#define SLIDE_DIRECTION_LEFT	0
#define SLIDE_DIRECTION_RIGHT	1
#define SLIDE_DIRECTION_UP		2
#define SLIDE_DIRECTION_DOWN	3

#define DEFAULT_CONTAINER_SLIDE_DIRECTION		SLIDE_DIRECTION_LEFT
#define DEFAULT_CONTAINER_SLIDE_TIME			50
#define DEFAULT_CONTAINER_SLIDE_AWAY_TIMEOUT	-1

class ShishaSlidingContainer : public ShishaContainer{
protected:
	class SlideTab : public BasicButton{
		protected:
			ShishaSlidingContainer* container;
		
			virtual void onRelease(string cursorID);
		
			virtual void drawArrowLeft(float x, float y, float w, float h);
			virtual void drawArrowRight(float x, float y, float w, float h);
			virtual void drawArrowUp(float x, float y, float w, float h);
			virtual void drawArrowDown(float x, float y, float w, float h);
			virtual void drawDefaultSymbol(float x, float y, float w, float h);
		
		public:
			SlideTab(ShishaSlidingContainer* container=NULL):BasicButton(){this->container = container;};
			~SlideTab(){};
			
			virtual void draw(float x, float y, float w, float h);
		
			virtual bool isActive();
	};
	
	int slideDirection, slideTime, slideAwayTimeout, slidOutTime;
	float slideMoveX, slideMoveY, cSlideX, cSlideY, slideDestX, slideDestY;
	bool slidOut, doneSlide, askBeforeSlide, waitingToSlide;
	SlideTab* slideTab;
	
	virtual void initSlideTab();
	
	virtual void setSlidOut(bool slidOut);
	virtual void setSlideMovement(float slideMoveX=0.0, float slideMoveY=0.0);
	virtual void setSlideDestination(float slideDestX=-1.0, float slideDestY=-1.0);
	
	virtual void startSlide();
	virtual void endSlide();
	virtual bool isDoneSliding();
	
	virtual void slideTo(float x, float y, bool force=false);
	
public:
	ShishaSlidingContainer();
	~ShishaSlidingContainer();
	virtual void init(float x=0, float y=0, float width=DEFAULT_GUI_ELEMENT_WIDTH, float height=DEFAULT_GUI_ELEMENT_HEIGHT, int slideDirection=DEFAULT_CONTAINER_SLIDE_DIRECTION);
	
	bool isSlidOut();
	bool isSliding();
	int getSlideDirection();
	float getSlideX();
	float getSlideY();
	
	virtual bool isReadyToSlide();
	virtual bool shouldSlideAway();
	bool isWaitingToSlide();
	void doSlide();
	
	virtual ShishaElement* getElement(string selector=DEFAULT_SELECTOR, string& subSelector="");
	
	virtual void tabMoved(float xMove, float yMove);
	
	virtual void setGUIState(int guiState);
	virtual void setAskBeforeSlide(bool askBeforeSlide=true);
	virtual void setPosition(float x, float y);
	virtual void setSlideDirection(int slideDirection=DEFAULT_CONTAINER_SLIDE_DIRECTION);
	virtual void setSlideTime(int slideTime=DEFAULT_CONTAINER_SLIDE_TIME);
	virtual void setSlideAwayTimeout(int slideAwayTimeout=DEFAULT_CONTAINER_SLIDE_AWAY_TIMEOUT);
	
	virtual bool slideIn(bool force=false);
	virtual bool slideOut(bool force=false);
	virtual bool slideToggle(bool force=false);
	
	virtual void draw();
	virtual void draw(float x, float y, float w, float h, bool borders);
	virtual void update();
	
	virtual bool checkCursorHover(int x, int y, string cursorID, float w=1.0, float h=1.0);
};

#endif
