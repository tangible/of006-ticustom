/*
 *  SliderBar.cpp
 *  openFrameworks
 *
 *  Created by Pat Long on 13/09/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */

#include "SliderBar.h"

SliderBar::SliderBar(){
}

SliderBar::~SliderBar(){
}

void SliderBar::init(float x, float y, float width, float height){
	ShishaContainer::init(x, y, width, height);
	this->setName("SliderBar");
	this->setLabel("Slider Bar");
	this->elementType = SHISHA_TYPE_HOOKAH_SLIDER_BAR;
	this->setSliderWidthPercent();
	this->initBar(x, y, width, height);
	this->initSlider();
}

float SliderBar::getSliderWidth(){
	return this->sliderWidth;
}

void SliderBar::setSliderWidth(float sliderWidth){
	if(sliderWidth <= 0.0)
		sliderWidth = this->getWidth() * this->sliderWidthPercent;
	this->sliderWidth = sliderWidth;
}

void SliderBar::setSliderWidthPercent(float sliderWidthPercent){
	this->sliderWidthPercent = sliderWidthPercent;
	this->setSliderWidth();
}

void SliderBar::initBar(float x, float y, float width, float height){
	this->bar = new Bar();
	this->bar->init(x, y, width, height);
	this->bar->setFillColour(SHISHA_COLOUR_FOREGROUND_1);
	this->addElement(this->bar, false);
}

int SliderBar::getInt(string selector){
	return this->bar->getInt(selector);
}

float SliderBar::getFloat(string selector){
	if(selector == SELECTOR_SLIDER_WIDTH)
		return this->getSliderWidth();
	else
		return this->bar->getFloat(selector);
}

float SliderBar::getPercentage(){
	return this->bar->getPercentage();
}

void SliderBar::setInt(int value){
	this->bar->setInt(value);
	this->slider->refreshLocation();
}

void SliderBar::setMaxValue(int maxValue){
	this->bar->setMaxValue(maxValue);
	this->slider->refreshLocation();
}

void SliderBar::initSlider(){
	this->slider = new Slider();
	this->slider->init(this->x, this->y, this->width, this->height);
	this->addElement(this->slider, false);
}

SliderBar::Slider::Slider(){
}

SliderBar::Slider::~Slider(){
}

void SliderBar::Slider::init(float x, float y, float width, float height, int elementID){
	ShishaElement::init(x, y, width, height, elementID);
	this->setName("SliderBar:Slider");
	this->setLabel("Slider");
	this->elementType = SHISHA_TYPE_HOOKAH_SLIDER_BAR_SLIDER;
}

void SliderBar::Slider::drawAsRectangle(float x, float y, float w, float h, bool borders){
	this->getTheme()->setColour(SHISHA_COLOUR_FOREGROUND_2, this);
	ofFill();
	ofRect(x, y, w, h);
	
	if(borders){
		this->getTheme()->setColour(SHISHA_COLOUR_BORDER, this);
		ofNoFill();
		ofRect(x, y, w, h);
		ofFill();
	}
}

void SliderBar::Slider::drawAsCircle(float x, float y, float w, float h, bool borders){
	float radius = w/2.0;
	
	this->getTheme()->setColour(SHISHA_COLOUR_FOREGROUND_2, this);
	ofFill();
	ofCircle(x+radius, y+radius, radius);
	
	if(borders){
		this->getTheme()->setColour(SHISHA_COLOUR_BORDER, this);	
		ofNoFill();
		ofCircle(x+radius, y+radius, radius);
		ofFill();
	}
}

void SliderBar::Slider::onDrag(string cursorID){
	cout << "SliderDrag:" << cursorID << ":" << this->cursors.getMoveX(cursorID) << "," << this->cursors.getMoveY(cursorID) << ":" << endl;
	// TODO: decide how to notify parent .... I'm thinking this is where the event/listener system is going to be *required*
}

void SliderBar::Slider::setParent(ShishaElement* parent){
	ShishaElement::setParent(parent);
	this->refreshDimensions();
	this->refreshLocation();
}

void SliderBar::Slider::refreshDimensions(){
	if(this->parent == NULL)
		return;
	
	this->width = this->parent->getFloat(SELECTOR_SLIDER_WIDTH);
	this->height = this->parent->getHeight();
}

void SliderBar::Slider::refreshLocation(){
	if(this->parent == NULL)
		return;
	this->x = this->parent->getX() + this->parent->getWidth()*this->parent->getFloat(SELECTOR_PERCENTAGE) - this->width / 2.0;
	this->y = this->parent->getY();
	
	float minX = this->parent->getX();
	float maxX = this->parent->getX() + this->parent->getWidth() - this->width;
	if(this->x < minX)
		this->x = minX;
	else if(this->x > maxX)
		this->x = maxX;
}
