/*
 *  Bar.cpp
 *  openFrameworks
 *
 *  Created by Pat Long on 13/09/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */
#include "Bar.h"

Bar::Bar(){
}

Bar::~Bar(){
}

void Bar::init(float x, float y, float width, float height, int elementID){
	ShishaElement::init(x, y, width, height, elementID);
	this->elementType = SHISHA_TYPE_HOOKAH_BAR;
	this->setName("Bar");
	this->setLabel("Bar");
	this->setMaxValue();
	this->setInt();
	this->setFillColour();
}


void Bar::drawAsRectangle(float x, float y, float w, float h, bool borders){
	this->getTheme()->setColour(SHISHA_COLOUR_BACKGROUND, this);
	ofFill();
	ofRect(x, y, w, h);
	
	this->getTheme()->setColour(this->fillColour, this);
	ofRect(x, y, this->getPercentage()*w, h);
	
	if(borders){
		this->getTheme()->setColour(SHISHA_COLOUR_BORDER, this);
		ofNoFill();
		ofRect(x, y, w, h);
		ofFill();
	}
}

void Bar::drawAsCircle(float x, float y, float w, float h, bool borders){
	float radius = w/2.0;
	float innerRadius = h/2.0;
	
	this->getTheme()->setColour(SHISHA_COLOUR_BACKGROUND, this);
	ofFill();
	ofCircle(x+radius, y+radius, radius);
	
	this->getTheme()->setColour(this->fillColour, this);
	ofCircleSlice(x+radius, y+radius, radius, 0.0, 360.0*this->getPercentage());
	
	this->getTheme()->setColour(SHISHA_COLOUR_FOREGROUND_1, this);
	ofCircle(x+radius, y+radius, innerRadius);
	
	if(borders){
		this->getTheme()->setColour(SHISHA_COLOUR_BORDER, this);	
		ofNoFill();
		ofCircle(x+radius, y+radius, radius);
		ofFill();
	}
}

int Bar::getInt(string selector){
	if(this->selectSelf(selector))
		return this->value;
	return SELECTOR_UNIDENTIFIED_INT;
}

float Bar::getFloat(string selector){
	if(this->selectSelf(selector))
		return (float)this->value;
	else if(selector == SELECTOR_PERCENTAGE);
		return this->getPercentage();
	return SELECTOR_UNIDENTIFIED_FLOAT;
}

float Bar::getPercentage(){
	return ((float)this->value / (float)this->maxValue);
}

void Bar::setInt(int value){
	this->value = value;
}

void Bar::setMaxValue(int maxValue){
	this->maxValue = maxValue;
}

void Bar::setFillColour(int fillColour){
	this->fillColour = fillColour;
}
