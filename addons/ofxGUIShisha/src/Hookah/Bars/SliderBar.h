/*
 *  SliderBar.h
 *  openFrameworks
 *
 *  Created by Pat Long on 13/09/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */
#ifndef _OFX_GUISHISHA_SLIDERBAR
#define _OFX_GUISHISHA_SLIDERBAR

#include "ShishaContainer.h"
#include "Bar.h"

#define DEFAULT_SLIDER_WIDTH_PERCENT 0.1

#define SELECTOR_SLIDER_WIDTH "sliderWidth"

class SliderBar : public ShishaContainer{
protected:
	class Slider : public ShishaElement{
		protected:
			virtual void drawAsRectangle(float x, float y, float w, float h, bool borders);
			virtual void drawAsCircle(float x, float y, float w, float h, bool borders);
		
			virtual void onDrag(string cursorID);
		
		public:
			Slider();
			~Slider();
		
			virtual void init(float x=0, float y=0, float width=DEFAULT_GUI_ELEMENT_WIDTH, float height=DEFAULT_GUI_ELEMENT_HEIGHT, int elementID=-1);
			virtual	void setParent(ShishaElement* parent);
		
			virtual void refreshDimensions();
			virtual void refreshLocation();
	};
	
	Bar* bar;
	Slider* slider;
	float sliderWidth, sliderWidthPercent;
	
	void initBar(float x, float y, float width, float height);
	void initSlider();
	
public:
	SliderBar();
	~SliderBar();
	
	virtual void init(float x=0, float y=0, float width=DEFAULT_GUI_ELEMENT_WIDTH, float height=DEFAULT_GUI_ELEMENT_HEIGHT);
	
	virtual float getSliderWidth();
	virtual void setSliderWidth(float sliderWidth=-1.0);
	virtual void setSliderWidthPercent(float sliderWidthPercent=DEFAULT_SLIDER_WIDTH_PERCENT);
	
	/** Bar Accessors */
	virtual int getInt(string selector=DEFAULT_SELECTOR);
	virtual float getFloat(string selector=DEFAULT_SELECTOR);
	virtual float getPercentage();
	
	virtual void setInt(int value=0);
	virtual void setMaxValue(int maxValue=DEFAULT_MAX_VALUE);
	
};

#endif
