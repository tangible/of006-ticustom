#include "testApp.h"

//--------------------------------------------------------------
void testApp::setup(){
	int windowMode = ofGetWindowMode();
	this->windowBounds.x = this->windowBounds.y = 0;
	if(windowMode == OF_FULLSCREEN){
		this->windowBounds.width = ofGetScreenWidth();
		this->windowBounds.height = ofGetScreenHeight();
	}
	else if(windowMode == OF_WINDOW){
		this->windowBounds.width = ofGetWidth();
		this->windowBounds.height = ofGetHeight();
	}
	
	ofSetFrameRate(60);
	ofSetBackgroundAuto(true);
	ofEnableAlphaBlending();
//	ofBackground(0, 0, 0);									// TRY: default theme with black background
	ofBackground(255, 255, 255);
	
	this->tuio = new myTuioClient();
	ofAddListener(this->tuio->cursorAdded,this,&testApp::tuioAdded);
	ofAddListener(this->tuio->cursorRemoved,this,&testApp::tuioRemoved);
	ofAddListener(this->tuio->cursorUpdated,this,&testApp::tuioUpdated);
	this->tuio->start(3333);
	
	/** if the container or panel is too small to hold newly added elements, it will be automatically resized
	 If it's in SHISHA_FLOW_MODE_HORIZONTAL flow mode, it will scale vertically, adding new rows as needed
	 If it's in SHISHA_FLOW_MODE_VERTICAL flow mode, it will scale horizontally, adding new columns as needed
	 */
	float width = 250;
	float height = 170;

	myShisha.init(10, 10, width*2.0+30, height, "GUI Shisha Test", "GUI");
	myShisha.setTheme();									// TRY: implementing your own theme
	myShisha.setHorizontalSpacing(10);
	myShisha.setVerticalSpacing(10);

	ShishaPanel* newPanel = new ShishaPanel();
	newPanel->init(0, 0, width, height, "Input", "INPUT");
//	newPanel->setFlowMode(SHISHA_FLOW_MODE_VERTICAL);		// TRY: changing to vertical flow mode (probably will need to increase myShisha's width)
	myShisha.addElement(newPanel);

	BasicButton* newButton;
	newButton = newPanel->addButton(100, 25);
	newButton->setName("button1");
	
	newButton = newPanel->addToggleButton(80, 25);
	newButton->setName("toggle1");
	newButton->setButtonStyle(BUTTON_STYLE_LABEL_LEFT);
	((ToggleButton*)newButton)->setToggleStyle(BUTTON_STYLE_TOGGLE_CROSS);
	
	newButton = newPanel->addButton(25, 25);
	newButton->setName("button2");
	newButton->setGUIShape(GUI_ELEMENT_SHAPE_CIRCLE);		// TRY: changing the GUI shape of elements
	newButton->setLabel("");
	
	RadioSet* newRadioSet;
	newRadioSet = newPanel->addRadioSet(240, 100);
	newRadioSet->setName("radio1");
	newRadioSet->setFlowMode(SHISHA_FLOW_MODE_VERTICAL);
	newRadioSet->setButtonShape(GUI_ELEMENT_SHAPE_CIRCLE);	// TRY: changing the GUI shape of a radio set
	newButton = (BasicButton*)newRadioSet->addButton(112, 25);
	newButton->setName("item1");
	newButton = (BasicButton*)newRadioSet->addButton(112, 25);
	newButton->setName("item2");
	newButton = (BasicButton*)newRadioSet->addButton(112, 25);
	newButton->setName("item3");
	newButton = (BasicButton*)newRadioSet->addButton(112, 25);
	newButton->setName("item4");
	newButton = (BasicButton*)newRadioSet->addButton(112, 25);
	newButton->setName("item5");
	
	newPanel = new ShishaPanel();
	newPanel->init(0, 0, width, height, "Output", "OUTPUT");
	newPanel->setFlowMode(SHISHA_FLOW_MODE_VERTICAL); // try it and see :)
	myShisha.addElement(newPanel);
	
	TextOutput* newTextOut;
	newTextOut = newPanel->addTextOutput(240, 25);
	newTextOut->setName("button1Status");
	newTextOut->setLabel("Button: ");
	
	newTextOut = newPanel->addTextOutput(240, 25);
	newTextOut->setName("toggle1Status");
	newTextOut->setLabel("Toggle: ");
	
	newTextOut = newPanel->addTextOutput(240, 25);
	newTextOut->setName("button2Status");
	newTextOut->setLabel("Circle: ");
	
	newTextOut = newPanel->addTextOutput(240, 25);
	newTextOut->setName("radio1Status");
	newTextOut->setLabel("Radio: ");
	
	float x = (this->windowBounds.width - width*2.0 + 10)/ 2.0;
	float y = (this->windowBounds.height - height) / 2.0 - 50;
	myShisha.setLocation(x, y, -1, -1, 50); // check it out, it can move around!
}

//--------------------------------------------------------------
void testApp::update(){
	this->tuio->getMessage();
	myShisha.update();
	
	myShisha.setValue("OUTPUT.button1Status.value", (myShisha.getBool("INPUT.button1.value")?"FIRE!":"inactive"));
	myShisha.setValue("OUTPUT.toggle1Status", (myShisha.getBool("INPUT.toggle1.value")?"On":"Off"));
	myShisha.setValue("OUTPUT.button2Status", (myShisha.getBool("INPUT.button2.value")?"On":"Off"));
	myShisha.setValue("OUTPUT.radio1Status", myShisha.getString("INPUT.radio1.value"));
}

//--------------------------------------------------------------
void testApp::draw(){
	this->tuio->drawCursors();
	myShisha.draw();
}

//--------------------------------------------------------------
void testApp::keyPressed(int key){

}

//--------------------------------------------------------------
void testApp::keyReleased(int key){

}

//--------------------------------------------------------------
void testApp::mouseMoved(int x, int y ){
	this->myShisha.checkCursorHover(x, y, MOUSE_CURSOR_ID);
}

//--------------------------------------------------------------
void testApp::mouseDragged(int x, int y, int button){
	this->myShisha.checkCursorDrag(x, y, button, MOUSE_CURSOR_ID);
}

//--------------------------------------------------------------
void testApp::mousePressed(int x, int y, int button){
	this->myShisha.checkCursorPress(x, y, button, MOUSE_CURSOR_ID);
}

//--------------------------------------------------------------
void testApp::mouseReleased(int x, int y, int button){
	this->myShisha.checkCursorRelease(x, y, button, MOUSE_CURSOR_ID);
}

//--------------------------------------------------------------
void testApp::tuioAdded(ofxTuioCursor & tuioCursor){
	tuioCursor.scaleTo(&this->windowBounds);
	this->myShisha.checkCursorPress(tuioCursor.getX(), tuioCursor.getY(), 0, tuioCursor.getCursorID());
	tuioCursor.scaleFrom(&this->windowBounds);
}

//--------------------------------------------------------------
void testApp::tuioRemoved(ofxTuioCursor & tuioCursor){
	tuioCursor.scaleTo(&this->windowBounds);
	this->myShisha.checkCursorRelease(tuioCursor.getX(), tuioCursor.getY(), 0, tuioCursor.getCursorID());
	tuioCursor.scaleFrom(&this->windowBounds);
}

//--------------------------------------------------------------
void testApp::tuioUpdated(ofxTuioCursor & tuioCursor){
	tuioCursor.scaleTo(&this->windowBounds);
	this->myShisha.checkCursorDrag(tuioCursor.getX(), tuioCursor.getY(), 0, tuioCursor.getCursorID());
	tuioCursor.scaleFrom(&this->windowBounds);
}

//--------------------------------------------------------------
void testApp::windowResized(int w, int h){
	int windowMode = ofGetWindowMode();
	this->windowBounds.x = this->windowBounds.y = 0;
	if(windowMode == OF_FULLSCREEN){
		this->windowBounds.width = ofGetScreenWidth();
		this->windowBounds.height = ofGetScreenHeight();
	}
	else if(windowMode == OF_WINDOW){
		this->windowBounds.width = ofGetWidth();
		this->windowBounds.height = ofGetHeight();
	}
}

