/*
 *  I2WGUI_TabContainer.h
 *  openFrameworks
 *
 *  Created by Pat Long on 02/03/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */
#ifndef _I2WGUI_TABCONTAINER_H
#define _I2WGUI_TABCONTAINER_H

#include "ofxThread.h"
#include "ofxGUIShisha.h"
#include "ofxImage2Web.h"
#include "I2WGUI_TextInput.h";
#include "I2WGUI_PasswordInput.h";

#define SHISHA_TYPE_I2WGUI_TAB_CONTAINER		100

#define I2WGUI_IMAGE_ROOT	"i2w_gui/"
#define I2WGUI_FORM_ROOT	I2WGUI_IMAGE_ROOT"interface/"
#define I2WGUI_ICON_ROOT	I2WGUI_IMAGE_ROOT"icons/"
#define I2WGUI_STATUS_ROOT	I2WGUI_FORM_ROOT"status_messages/"

class I2WGUI_TabContainer : public ShishaTabContainer{
protected:
	class I2WGUI_Tab : public ShishaTab{
	protected:

	public:
		I2WGUI_Tab(ShishaTabContainer* container=NULL):ShishaTab(container){};
		~I2WGUI_Tab(){};
		
		virtual void drawAsRectangle(float x, float y, float w, float h, bool borders);
	};
	
	class I2WGUI_TabContainerStatusUpdater : public ofxThread{
	protected:
		I2WGUI_TabContainer* container;
		Image2WebService* service;
		
		virtual void threadedFunction();
		
	public:
		I2WGUI_TabContainerStatusUpdater();
		~I2WGUI_TabContainerStatusUpdater();
		
		void start(Image2WebService* service=NULL);
		void stop();
		
		void setContainer(I2WGUI_TabContainer* container);
		void setService(Image2WebService* service);
	};
	
	I2W_Settings* settings;
	
	ofImage icon;
	bool iconLoaded;
	
	ImageList* statusImage;
	I2WGUI_TabContainerStatusUpdater statusUpdater;
	ColourFader statusFader;
	
	ShishaImageMap* form;
	bool formLoaded;
	bool submitPressed, cancelPressed, closePressed;
	vector<string> formInputNames;
	
	ShishaKeyboard* mtKeyboard;
	
	ofImage* image;
	bool submitRequested;
	bool requestCapture;
	
	
	virtual void initContainerTab();
	virtual void initInterface();
	virtual void initMTKeyboard();
	
	virtual void animateHide();
	
	virtual void sliceInterface()=0;
	virtual void doCancel()=0;
	virtual void doSubmit()=0;
	
	virtual void drawChildren();
	
	virtual void updateMTKeyboard();
	
public:
	I2WGUI_TabContainer();
	~I2WGUI_TabContainer();
	
	virtual void init(float x=0, float y=0, float width=DEFAULT_GUI_ELEMENT_WIDTH, float height=DEFAULT_GUI_ELEMENT_HEIGHT, string name="", int elementID=-1);
	virtual void update();
	
	ofImage* getIcon();
	ShishaKeyboard* getKeyboard();
	virtual string getServiceName()=0;
	
	ofImage* getImage();
	virtual void setSettings(I2W_Settings* settings);
	void setImage(ofImage* image);
	void setStatusImage(string imageName);
	virtual void setDefaultFormValues()=0;

	
	//---------------------- %DENNIS% changes:
	bool	autoClose;
	int     closeCounterNow;
	int		closeCounterLast;
	int		closeCounterPeriod;
	
	virtual void logData();
	

	//----------------------------
	/**
	virtual bool checkCursorHover(int x, int y, string cursorID, float w=1.0, float h=1.0);
	virtual bool checkCursorDrag(int x, int y, int button, string cursorID, float w=1.0, float h=1.0);
	virtual bool checkCursorPress(int x, int y, int button, string cursorID, float w=1.0, float h=1.0);
	virtual bool checkCursorRelease(int x, int y, int button, string cursorID, float w=1.0, float h=1.0);
	*/
	virtual bool checkKeyPressed(int key);
	virtual bool checkKeyReleased(int key);
	
	virtual bool getBool(string selector=DEFAULT_SELECTOR);
};

#endif
