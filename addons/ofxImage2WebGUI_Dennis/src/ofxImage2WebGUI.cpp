/*
 *  ofxImage2WebGUI.cpp
 *  openFrameworks
 *
 *  Created by Pat Long on 02/03/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#include "ofxImage2WebGUI.h"

ofxImage2WebGUI::ofxImage2WebGUI(){
}

ofxImage2WebGUI::~ofxImage2WebGUI(){
}

void ofxImage2WebGUI::init(float x, float y, float width, float height){
	ShishaTabManager::init(x, y, width, height);
	this->setName("i2w_manager");
	

	//---------------------- %DENNIS% changes:
	
	string path = settings.getDataRoot()+ "/i2w_gui/interface/";
	if (backGroundPic.loadImage(path + "fordText.png") )
	{
		cout << "loaded ford background Pic correctly" << endl;	
	}
	else {
		cout << "failed to load ford Image correctly" << endl;	
		abort();
	}
	//------------------------------------------
	
	I2WGUI_Theme* myTheme = new I2WGUI_Theme();
	myTheme->setBasePath(settings.getDataRoot());
	this->setTheme(myTheme);
	


		
}

void ofxImage2WebGUI::addContainer(ShishaTabContainer* container, bool inheritAnimation){
	if(container->getElementType() == SHISHA_TYPE_I2WGUI_TAB_CONTAINER){
		I2WGUI_TabContainer* i2wContainer = (I2WGUI_TabContainer*)container;
		if(i2wContainer->getServiceName() == "Twitter" && !settings.isTwitterEnabled())
			delete container;
		else if(i2wContainer->getServiceName() == "Email" && !settings.isEmailEnabled())
			delete container;
		else{
			i2wContainer->setSettings(&settings);
			ShishaTabManager::addContainer(container, inheritAnimation);
		}
	}
	else
		ShishaTabManager::addContainer(container, inheritAnimation);
}

void ofxImage2WebGUI::drawChildren(){
	int counter = 0;
	
	for(map<int,ShishaElement*>::iterator it=this->children.begin() ; it != this->children.end(); it++){
		counter++;
		
		
		if(this->activeContainer == NULL || ((*it).second != this->activeContainer->getContainerTab()))
		{
			((ShishaElement*)(*it).second)->draw();
		}
		
		//---------------------- %DENNIS% changes:
		else {
			backGroundPic.draw(ofGetWidth()/2 - backGroundPic.width/2, ofGetHeight()/2 - backGroundPic.height/2 + 40	);
			//ofRect(ofGetScreenWidth()/2, ofGetScreenHeight()/2, 400, 200);
		}
		//----------------

	}
}

void ofxImage2WebGUI::loadSettings(string settingFileName){
	this->settings.loadSettings(settingFileName);
}

I2W_Settings* ofxImage2WebGUI::getSettings(){
	return &settings;
}

void ofxImage2WebGUI::setImage(ofImage* image){
	for(int i=0; i < this->containers.size(); i++){
		if(this->containers[i]->getElementType() == SHISHA_TYPE_I2WGUI_TAB_CONTAINER)
			((I2WGUI_TabContainer*)this->containers[i])->setImage(image);
	}
}
