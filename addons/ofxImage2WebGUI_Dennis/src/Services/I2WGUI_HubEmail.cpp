/*
 *  I2WGUI_HubEmail.cpp
 *  graffitiwall 7.3
 *
 *  Created by Eli Smiles on 11-04-18.
 *  Copyright 2011 tangibleInteraction. All rights reserved.
 *
 */

#include "I2WGUI_HubEmail.h"

I2WGUI_HubEmail::I2WGUI_HubEmail(){
	this->form = NULL;
	this->submitWait = -1;
	formLoaded = false;
}

I2WGUI_HubEmail::~I2WGUI_HubEmail(){
}

void I2WGUI_HubEmail::sliceInterface(){
	if(this->formLoaded && this->form != NULL){
		
		string dataRoot = "";
		if(this->settings != NULL)
			dataRoot = this->settings->getDataRoot();
		
		string baseDefaultImage = "";
		stringstream fileBuilder;
		fileBuilder << dataRoot << I2WGUI_FORM_ROOT << "form_" << this->getServiceName() << "_default_";
		baseDefaultImage = fileBuilder.str();
		fileBuilder.str("");
		
		int xOffset = 30;
		int yOffset = 10;
		
		BasicButton* newButton;
		newButton = new BasicButton();
		newButton->init(xOffset + 349,yOffset + 58, 92, 33);
		newButton->setName("Submit");
		this->form->mapElement(newButton, xOffset +349, yOffset +58, 92, 33);
		
		newButton = new BasicButton();
		newButton->init(xOffset +448, yOffset +59, 92, 33);
		newButton->setName("Cancel");
		this->form->mapElement(newButton, xOffset +448, yOffset +59, 92, 33);
		
		newButton = new BasicButton();
		newButton->init(xOffset +511, yOffset +18, 29, 29);
		newButton->setName("Close");
		this->form->mapElement(newButton, xOffset + 511, yOffset +18, 29, 29);
		
		I2WGUI_TextInput* newTextInput;
		newTextInput = new I2WGUI_TextInput();
		//		newTextInput->init(1, 41, 165, 31);
		newTextInput->init(xOffset +1, yOffset +19, 340, 33);
		newTextInput->setName("email_from");
		newTextInput->setDefaultValue("Your Email");		
		fileBuilder << baseDefaultImage << "email_from.png";
		newTextInput->setDefaultValueImage(fileBuilder.str());
		fileBuilder.str("");
		newTextInput->setPadding(6, 3, -3);
		//		this->form->mapElement(newTextInput, 1, 41, 165, 31, true, true);
		this->form->mapElement(newTextInput, xOffset +1, yOffset +19, 340, 33, true, true);
		
		newTextInput->setMaxLineCount(1);
		newTextInput->setMaxTextLength(-1);
		formInputNames.push_back("email_from");
		
		newTextInput = new I2WGUI_TextInput();
		//		newTextInput->init(170, 41, 165, 33);
		newTextInput->init(xOffset +1, yOffset +58, 340, 34);
		newTextInput->setName("email_to");
		newTextInput->setDefaultValue("Friend's Email");
		fileBuilder << baseDefaultImage << "email_to.png";
		newTextInput->setDefaultValueImage(fileBuilder.str());
		fileBuilder.str("");
		newTextInput->setPadding(6, 3, -3);
		//		this->form->mapElement(newTextInput, 170, 41, 165, 31, true, true);
		this->form->mapElement(newTextInput, xOffset +1, yOffset +57, 340, 34, true, true);
		newTextInput->setMaxLineCount(1);
		newTextInput->setMaxTextLength(-1);
		formInputNames.push_back("email_to");
		
		if(settings == NULL || (settings != NULL && settings->isEmailMessageGUIEnabled())){
			newTextInput = new I2WGUI_TextInput();
			newTextInput->init(xOffset +1, yOffset +97, 540, 34);
			newTextInput->setName("message");
			newTextInput->setDefaultValue("Message");
			fileBuilder << baseDefaultImage << "email_message.png";
			newTextInput->setDefaultValueImage(fileBuilder.str());
			fileBuilder.str("");
			newTextInput->setPadding(6, 3, -3);
			this->form->mapElement(newTextInput, xOffset +1, yOffset + 97, 540, 34, true, true);
			newTextInput->setMaxLineCount(1);
			newTextInput->setMaxTextLength(-1);
			formInputNames.push_back("message");
		}
		this->setDefaultFormValues();
	}
}

void I2WGUI_HubEmail::setDefaultFormValues(){
	if(this->formLoaded && this->form != NULL){
		this->form->setStringValue("email_from", "Your Email");
		this->form->setStringValue("email_to", "Friend's Email");
		if(settings->isEmailMessageGUIEnabled())
			this->form->setStringValue("message", "Message");
	}
}

void I2WGUI_HubEmail::doCancel(){
	this->setDefaultFormValues();
	if(this->statusImage != NULL)
		this->statusImage->setActiveImage("");
}

void I2WGUI_HubEmail::update(){
	I2WGUI_TabContainer::update();
	
	if(this->submitWait != -1 && ofGetFrameNum() >= this->submitWait){
		if(this->doSubmit1())
			this->submitWait = -1;
	}
}

void I2WGUI_HubEmail::doSubmit(){
	if(this->submitWait == -1 && !this->statusUpdater.isThreadRunning() && this->formLoaded && this->form != NULL && this->image != NULL){
		this->setStatusImage("processing");
		this->submitWait = ofGetFrameNum()+1;
	}
}

bool I2WGUI_HubEmail::doSubmit1(){
	if(!this->statusUpdater.isThreadRunning() && this->formLoaded && this->form != NULL && this->image != NULL && !this->requestCapture){
		bool result = false;
		
		string email_from = this->form->getString("email_from");
		string email_to = this->form->getString("email_to");
		string email_subject = settings->getEmailSubject();
		string email_message = settings->getEmailDefaultMessage();
		if(settings->isEmailMessageGUIEnabled())
			email_message = this->form->getString("message");
		stringstream messageBuilder;
		messageBuilder << email_message << endl;
		messageBuilder << settings->getEmailMessageAppend();
		email_message = stringReplace(messageBuilder.str(), "\r\n", "\n");
		messageBuilder.str("");
		
		bool validFields = true;

		if(email_from == "" && email_to != "")
			email_from = email_to;
		else if(email_to == "" && email_from != "")
			email_to = email_from;
		
		if(!validEmail(email_from)){
			messageBuilder << "I2W_HubEmail: invalid email address set for 'sender' parameter (" << email_from << ")";
			ofLog(OF_LOG_ERROR, messageBuilder.str());
			messageBuilder.str("");
			validFields = false;
		}
		
		if(!validEmail(email_to)){
			messageBuilder << "I2W_HubEmail: invalid email address set for 'recipient' parameter (" << email_to << ")";
			ofLog(OF_LOG_ERROR, messageBuilder.str());
			messageBuilder.str("");
			validFields = false;
		}
		
		if(validFields){
			string imageName = this->image->getFileName();
			if(imageName == "")
				imageName = settings->getHubSavePath()+settings->getImageName();
			
			imageName = imageName.substr(0 , imageName.length() - 3)  + "jpg";
			//		cout << "image name: " << imageName << endl;		
			//		this->image->saveImage(imageName);
			
			if(this->image->saveJpg(imageName, 92)){
				ofxTI_HubXmlEntry* newEntry = new ofxTI_HubXmlEntry(&hub);
				
				log_sender =  email_from;
				log_recipient = email_to;
				
				newEntry->addTextNode(email_from, "from");
				newEntry->addTextNode(email_to, "to");
				newEntry->addTextNode(email_subject, "subject");
				newEntry->addTextNode(email_message, "message");
				newEntry->addImageNode(*image);
				
//				newEntry->saveEntryXml();
				
				delete newEntry;
				
				result = true;
			}
		}
			
		if(result)
			this->setStatusImage("success");
		else
			this->setStatusImage("error");
		
		return true;
	}
	return false;
}

void I2WGUI_HubEmail::logData(){
	
	//------------------output email here:
	string 	message = "loading xml data";
	cout << message << endl;
	
	string path = this->settings->getDataRoot() + "/../";
	cout << path << endl;
	
	
	//we load our settings file
	if( mailList.loadFile(path+ "emails.xml") ){
		message = "emails.xml loaded!";
	}else{
		message = "unable to load emails.xml check data/ folder";
		
		mailList.addTag("data");
	}
	cout << message << endl;
	
	mailList.pushTag("data", 0);
	
	int target = mailList.addTag("email");
	mailList.setValue("email:sender",log_sender, target);
	mailList.setValue("email:recipient",log_recipient, target);
	cout << log_sender << endl;
	cout << log_recipient << endl;
	
	mailList.popTag();
	
	mailList.saveFile(path + "emails.xml");
	cout << endl << "saved XML FILE - mailList.xml" << endl;
	//----------------------------
}

bool I2WGUI_HubEmail::validEmail(string emailAddress){
	if(emailAddress.length() > 0){
		vector<string> emSplit = split(emailAddress, '@');
		if(emSplit.size() == 2){
			if(emSplit[0].length() > 0 && emSplit[1].length() > 4){
				vector<string> hostSplit = split(emSplit[1], '.');
				if(hostSplit.size() >= 2){
					int domainLength = hostSplit[hostSplit.size()-1].length();
					if(domainLength >= 2 && domainLength <= 3)
						return true;
				}
			}			
		}
	}
	return false;
}


