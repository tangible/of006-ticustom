/*
 *  I2WGUI_HubXml.cpp
 *  graffitiwall 7.3
 *
 *  Created by Eli Smiles on 11-04-18.
 *  Copyright 2011 tangibleInteraction. All rights reserved.
 *
 */

#include "I2WGUI_HubXml.h"

I2WGUI_HubXml::I2WGUI_HubXml(){
}

I2WGUI_HubXml::~I2WGUI_HubXml(){
}

void I2WGUI_HubXml::setSettings(I2W_Settings* settings){
	I2WGUI_TabContainer::setSettings(settings);
	if(settings != NULL){
		this->setHubSettings(settings->getHubInstallationID(), settings->getHubSavePath());
	}
}

void I2WGUI_HubXml::setHubSettings(int installationID, string savePath){
	hub.setInstallationID(installationID);
	hub.setSavePath(savePath);
}
