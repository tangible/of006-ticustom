/*
 *  ofxImage2WebGUIServiceIndex.h
 *  openFrameworks
 *
 *  Created by Pat Long on 02/03/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */
#ifndef _OFX_IMAGE2WEB_GUI_SERVICE_INDEX
#define _OFX_IMAGE2WEB_GUI_SERVICE_INDEX

#include "I2WGUI_Email.h"
#include "I2WGUI_Facebook.h"
#include "I2WGUI_Twitter.h"
#include "I2WGUI_HubXml.h"
#include "I2WGUI_HubEmail.h"

#endif
