/*
 *  I2WGUI_Twitter.cpp
 *  openFrameworks
 *
 *  Created by Pat Long on 04/03/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */
#include "I2WGUI_Twitter.h"

I2WGUI_Twitter::I2WGUI_Twitter(){
	this->twitgoo = new I2W_Twitgoo();
}

I2WGUI_Twitter::~I2WGUI_Twitter(){
	delete this->twitgoo;
}

void I2WGUI_Twitter::sliceInterface(){
	if(this->formLoaded && this->form != NULL){
		BasicButton* newButton;
		newButton = new BasicButton();
		newButton->init(349, 41, 92, 31);
		newButton->setName("Submit");
		this->form->mapElement(newButton, 349, 41, 92, 31);
		
		newButton = new BasicButton();
		newButton->init(448, 41, 92, 31);
		newButton->setName("Cancel");
		this->form->mapElement(newButton, 448, 41, 92, 31);
		
		newButton = new BasicButton();
		newButton->init(511, 2, 29, 29);
		newButton->setName("Close");
		this->form->mapElement(newButton, 511, 2, 29, 29);
		
		I2WGUI_TextInput* newTextInput;
		newTextInput = new I2WGUI_TextInput();
		newTextInput->init(1, 41, 165, 31);
		newTextInput->setName("username");
		newTextInput->setDefaultValue("User");
		newTextInput->setPadding(3, 3, -3);
		this->form->mapElement(newTextInput, 1, 41, 165, 31, true, true);
		newTextInput->setMaxLineCount(1);
		newTextInput->setMaxTextLength(-1);
		formInputNames.push_back("username");
		
		newTextInput = new I2WGUI_PasswordInput();
		newTextInput->init(170, 41, 165, 31);
		newTextInput->setName("password");
		newTextInput->setDefaultValue("Password");
		newTextInput->setPadding(3, 3, 2);
		this->form->mapElement(newTextInput, 170, 41, 165, 31, true, true);
		newTextInput->setMaxLineCount(1);
		newTextInput->setMaxTextLength(-1);
		formInputNames.push_back("password");
		
		newTextInput = new I2WGUI_TextInput();
		newTextInput->init(1, 81, 540, 31);
		newTextInput->setName("tweet");
		newTextInput->setDefaultValue("Tweet");
		newTextInput->setPadding(3, 3, -3);
		formInputNames.push_back("tweet");
		this->form->mapElement(newTextInput, 1, 81, 540, 31, true, true);
		newTextInput->setMaxLineCount(1);
		newTextInput->setMaxTextLength(-1);
		
		this->setDefaultFormValues();
	}
}

void I2WGUI_Twitter::setDefaultFormValues(){
	if(this->formLoaded && this->form != NULL){
		this->form->setStringValue("username", "User");
		this->form->setStringValue("password", "Password");
		this->form->setStringValue("tweet", "Tweet");
	}
}

void I2WGUI_Twitter::doCancel(){
	this->setDefaultFormValues();
	if(this->statusImage != NULL)
		this->statusImage->setActiveImage("");
}

void I2WGUI_Twitter::doSubmit(){
	if(!this->statusUpdater.isThreadRunning() && this->formLoaded && this->form != NULL && this->image != NULL){
		map<string,string> params;
		int result = -1;

		string imageName = this->image->getFileName();
		if(imageName == "")
			imageName = settings->getDataRoot()+settings->getImageName();
		this->image->saveImage(imageName);
		
		params["username"] = this->form->getString("username");
		params["password"] = this->form->getString("password");
		
		stringstream messageBuilder;
		string formMsg = this->form->getString("tweet");
		if(settings != NULL){
			if(formMsg == "") formMsg = settings->getTwitterDefaultMessage();
			messageBuilder << formMsg << settings->getTwitterMessageAppend() << endl;
		}
		else
			messageBuilder << formMsg << endl;
		params["message"] = messageBuilder.str();
		messageBuilder.str("");

		if(twitgoo->postImage(*(this->image), params)){
			this->setStatusImage("processing");
			statusUpdater.start(twitgoo);
		}
	}
}
