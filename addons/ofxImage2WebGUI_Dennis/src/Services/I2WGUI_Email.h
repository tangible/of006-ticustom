/*
 *  I2WGUI_Email.h
 *  openFrameworks
 *
 *  Created by Pat Long on 02/03/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */
#ifndef _OFX_IMAGE2WEB_GUI_SERVICE_EMAIL
#define _OFX_IMAGE2WEB_GUI_SERVICE_EMAIL

#include "I2WGUI_TabContainer.h"
#include "I2W_Email.h"

#define I2WGUI_EMAIL_DEFAULT_SMTP_SERVER	"mail.test.tangibleinteraction.com"
#define I2WGUI_EMAIL_DEFAULT_SMTP_PORT		26
#define I2WGUI_EMAIL_DEFAULT_SMTP_USERNAME	"test@test.tangibleinteraction.com"
#define I2WGUI_EMAIL_DEFAULT_SMTP_PASSWORD	"tang1ble"

class I2WGUI_Email : public I2WGUI_TabContainer{
protected:
	I2W_Email* email;
	int submitWait;
	
	string smtp_server;
	int smtp_port;
	string smtp_username;
	string smtp_password;

	
	virtual void sliceInterface();

	virtual void doCancel();
	virtual void doSubmit();
	
	void logData();
	
	void doSubmit1();
	
	//---------------%DENNIS%-------
	ofxXmlSettings mailList;
	string	log_sender;
	string  log_recipient;
	//--------------------------
	
public:
	I2WGUI_Email();
	~I2WGUI_Email();
	
	virtual void update();
	
	virtual string getServiceName(){ return "Email"; };
	
	virtual void setDefaultFormValues();
	virtual void setSettings(I2W_Settings* settings);
	
	void setSmtpParams(string smtp_server=I2WGUI_EMAIL_DEFAULT_SMTP_SERVER, int smtpPort=I2WGUI_EMAIL_DEFAULT_SMTP_PORT, string smtp_username=I2WGUI_EMAIL_DEFAULT_SMTP_USERNAME, string smtp_password=I2WGUI_EMAIL_DEFAULT_SMTP_PASSWORD);
};

#endif
