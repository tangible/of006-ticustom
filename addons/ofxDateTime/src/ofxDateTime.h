/*
 *  ofxDateTime
 *
 *  An addon for handling date and time functions.
 *  
 *  Created by Pat Long (plong0) on 29/09/10.
 *  Copyright 2010 Spiral Sense. All rights reserved.
 *
 */
#ifndef _OFX_DATE_TIME
#define _OFX_DATE_TIME

#include "ofMain.h"
#include <time.h>

#include "ofxDateTimeConstants.h"

class ofxDateTime{
protected:
	struct tm * dateTime;
	
public:
	ofxDateTime();
	~ofxDateTime();
	
	/**
	 specifier	Replaced by																	Example
	 %a			Abbreviated weekday name *													Thu
	 %A			Full weekday name *															Thursday
	 %b			Abbreviated month name *													Aug
	 %B			Full month name *															August
	 %c			Date and time representation *												Thu Aug 23 14:55:02 2001
	 %d			Day of the month (01-31)													23
	 %H			Hour in 24h format (00-23)													14
	 %I			Hour in 12h format (01-12)													02
	 %j			Day of the year (001-366)													235
	 %m			Month as a decimal number (01-12)											08
	 %M			Minute (00-59)																55
	 %p			AM or PM designation														PM
	 %S			Second (00-61)																02
	 %U			Week number with the first Sunday as the first day of week one (00-53)		33
	 %w			Weekday as a decimal number with Sunday as 0 (0-6)							4
	 %W			Week number with the first Monday as the first day of week one (00-53)		34
	 %x			Date representation *														08/23/01
	 %X			Time representation *														14:55:02
	 %y			Year, last two digits (00-99)												01
	 %Y			Year																		2001
	 %Z			Timezone name or abbreviation												CDT
	 %%			A % sign																	%
	 */
	string formatDateTime(string formatStr="%A, %B %d, %Y - %H:%M:%S");
	
	void refreshDateTime();
	
	int getDay();
	int getMonth();
	int getYear();
	int getHour();
	int getMinute();
	int getSecond();
	int getWeekday();
	
	int getEpoch();
	
	string getDateString(bool shortForm=false);
	string getDateTimeString(bool shortForm=false);
	string getMonthString(bool shortForm=false);
	string getTimeString();
	string getWeekdayString(bool shortForm=false);
	
	void setDate(int day, int month, int year);
	void setDay(int day);
	void setMonth(int month);
	void setYear(int year);
	
	void setTime(int hour, int minute, int second=0);
	void setHour(int hour);
	void setMinute(int minute);
	void setSecond(int second);
	
	void setFromEpoch(int epoch);
};

#endif
