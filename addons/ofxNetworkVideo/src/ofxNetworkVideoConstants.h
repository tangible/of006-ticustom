/*
 *  ofxNetworkVideoConstants.h
 *  openFrameworks
 *
 *  Created by Pat Long on 11/11/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */
#ifndef _OFX_NETWORK_VIDEO_CONSTANTS_H
#define _OFX_NETWORK_VIDEO_CONSTANTS_H

#define DEFAULT_NETWORK_VIDEO_BUFFER_SIZE	100000
#define DEFAULT_NETWORK_VIDEO_FRAME_WIDTH	160
#define DEFAULT_NETWORK_VIDEO_FRAME_HEIGHT	120
#define DEFAULT_NETWORK_VIDEO_IMAGE_TYPE	OF_IMAGE_COLOR
#define DEFAULT_NETWORK_VIDEO_FRAME_DELAY	10

#endif
