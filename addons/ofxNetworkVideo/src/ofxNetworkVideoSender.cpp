/*
 *  ofxNetworkVideoSender.cpp
 *  openFrameworks
 *
 *  Created by Pat Long on 11/11/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */

#include "ofxNetworkVideoSender.h"

ofxNetworkVideoSender::ofxNetworkVideoSender(){
	this->init();
}

ofxNetworkVideoSender::~ofxNetworkVideoSender(){
}

void ofxNetworkVideoSender::init(string hostName, int portNum, int width, int height, int imageType){
	ofxNetworkVideoComponent::init(portNum, width, height, imageType);
	this->hostName = hostName;
	this->setFrameDelay();
	
	if(this->hostName != "" && this->portNum > 0){
		this->Create();
		this->Connect(this->hostName.c_str(), this->portNum);
		this->SetNonBlocking(true);
		this->setBufferSize();
	}
}

void ofxNetworkVideoSender::update(){
	if(this->hasNewFrame()){
		this->sendVideoFrame(this->getPixels(), this->width, this->height, this->imageType);
	}
}

bool ofxNetworkVideoSender::isReadyToSend(){
	return (this->frameDelay == -1 || this->lastSendFrame == 0 || (ofGetFrameNum() - this->lastSendFrame) >= this->frameDelay);
}

void ofxNetworkVideoSender::setBufferSize(int bufferSize){
	this->SetSendBufferSize(bufferSize);
}

void ofxNetworkVideoSender::setFrameDelay(int frameDelay){
	this->frameDelay = frameDelay;
	this->lastSendFrame = 0;
}

void ofxNetworkVideoSender::setNewPixels(unsigned char* pixels, int width, int height, int type){
	ofImage temp;
	if(width != this->width || height != this->height || type != this->imageType){
		temp.setFromPixels(pixels, width, height, type);
		
		if(width != this->width || height != this->height){
			temp.resize(this->width, this->height);
			width = this->width;
			height = this->height;
		}
		
		if(type != this->imageType){
			temp.setImageType(this->imageType);
			type = this->imageType;
		}
		
		pixels = temp.getPixels();
	}
	
	ofxNetworkVideoComponent::setNewPixels(pixels, (width*height*this->bpp));
}


int ofxNetworkVideoSender::sendVideoFrame(unsigned char* pixels, int width, int height, int type){
	if(!this->isReadyToSend())
		return -1;
	ofImage temp;
	if(width != this->width || height != this->height || type != this->imageType){
		temp.setFromPixels(pixels, width, height, type);
		
		if(width != this->width || height != this->height){
		   temp.resize(this->width, this->height);
			width = this->width;
			height = this->height;
		}
		
		if(type != this->imageType){
			temp.setImageType(this->imageType);
			type = this->imageType;
		}
		
		pixels = temp.getPixels();
	}
	
	int pixelSize = width*height*this->bpp;
	
	stringstream messageBuilder;
	for(int i=0; i < pixelSize; i++)
		messageBuilder << pixels[i];
	
	string message = messageBuilder.str();
	messageBuilder.str("");
	
	int sent = this->Send(message.c_str(), message.length());
	cout << "sending video frame:" << message.length() << ":" << this->GetMaxMsgSize() << ":" << endl; //message << ":" << endl;
	this->lastSendFrame = ofGetFrameNum();
	return sent;
}
