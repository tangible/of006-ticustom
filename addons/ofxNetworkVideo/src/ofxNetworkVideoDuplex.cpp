/*
 *  ofxNetworkVideoDuplex.cpp
 *  openFrameworks
 *
 *  Created by Pat Long on 11/11/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */
#include "ofxNetworkVideoDuplex.h"

ofxNetworkVideoDuplex::ofxNetworkVideoDuplex(){
}

ofxNetworkVideoDuplex::~ofxNetworkVideoDuplex(){
}

void ofxNetworkVideoDuplex::init(string hostName, int sendPort, int receivePort, int width, int height, int imageType){
	this->initSender(hostName, sendPort, width, height, imageType);
	this->initReceiver(receivePort, width, height, imageType);
}

void ofxNetworkVideoDuplex::initSender(string hostName, int portNum, int width, int height, int imageType){
	this->sender.init(hostName, portNum, width, height, imageType);
}

void ofxNetworkVideoDuplex::initReceiver(int portNum, int width, int height, int imageType){
	this->receiver.init(portNum, width, height, imageType);
}

void ofxNetworkVideoDuplex::setBufferSizes(int bufferSize){
	this->sender.setBufferSize(bufferSize);
	this->receiver.setBufferSize(bufferSize);
}

void ofxNetworkVideoDuplex::update(){
	this->sender.update();
	this->receiver.update();
}

int ofxNetworkVideoDuplex::getReceivedWidth(){
	return this->receiver.getWidth();
}

int ofxNetworkVideoDuplex::getReceivedHeight(){
	return this->receiver.getHeight();
}

int ofxNetworkVideoDuplex::getReceivedImageType(){
	return this->receiver.getImageType();
}

unsigned char* ofxNetworkVideoDuplex::getReceivedPixels(){
	return this->receiver.getPixels();
}

bool ofxNetworkVideoDuplex::hasReceivedNewFrame(){
	return this->receiver.hasNewFrame();
}

void ofxNetworkVideoDuplex::setSenderFrameDelay(int frameDelay){
	this->sender.setFrameDelay(frameDelay);
}

void ofxNetworkVideoDuplex::setNewSenderPixels(unsigned char* pixels, int width, int height, int type){
	this->sender.setNewPixels(pixels, width, height, type);
}

/**
int ofxNetworkVideoDuplex::sendVideoFrame(unsigned char* pixels, int width, int height, int type){
	return this->sender.sendVideoFrame(pixels, width, height, type);
}*/
