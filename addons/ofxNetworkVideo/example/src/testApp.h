#ifndef _TEST_APP
#define _TEST_APP

#include "ofMain.h"

#include "ofxNetworkVideo.h"
#include "ofxXmlSettings.h"

class testApp : public ofBaseApp{
	protected:
		ofxNetworkVideoDuplex netVideo;
	
		ofVideoGrabber localVideo;
		ofImage localImage;
		ofImage remoteImage;

	public:
		void setup();
		void update();
		void draw();

		void keyPressed  (int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void windowResized(int w, int h);

};

#endif
