/*
 *  ofxTI_HubXml.cpp
 *
 *  Created by Pat Long (plong0) on 11-01-26.
 *  Copyright 2011 Tangible Interaction. All rights reserved.
 *
 */

#include "ofxTI_HubXml.h"

ofxTI_HubXml::ofxTI_HubXml(int installationID, string savePath, string defaultImageExtension, TimeStampMode tsMode){
	this->setAuthenticationCode();
	this->setInstallationID(installationID);
	this->setSavePath(savePath);
	this->setDefaultImageExtension(defaultImageExtension);
	this->setTimeStampMode(tsMode);
}

ofxTI_HubXml::~ofxTI_HubXml(){
}

string ofxTI_HubXml::getAuthenticationCode(){
	return this->authenticationCode;
}

int ofxTI_HubXml::getInstallationID(){
	return this->installationID;
}

string ofxTI_HubXml::getNewEntryName(){
	stringstream nameBuilder;
	nameBuilder << this->getSavePath() << "entry_" << getTimeStamp(this->tsMode) << ".xml";
	return nameBuilder.str();
}

string ofxTI_HubXml::getNewImageName(string extension){
	if(extension == "")
		extension = this->defaultImageExtension;
	stringstream nameBuilder;
	nameBuilder << "image_" << getTimeStamp(TS_HUMAN) << "_" << ofGetElapsedTimeMillis() << "." << extension;
	return nameBuilder.str();
}

string ofxTI_HubXml::getSavePath(){
	string savePath = this->savePath;
	if(savePath.length() > 0 && savePath.at(savePath.length()-1) != '/')
		savePath += '/';
	return savePath;
}

string ofxTI_HubXml::getTimeStamp(TimeStampMode tsMode){
	stringstream timeStamp;
	if(tsMode == TS_HUMAN)
		timeStamp << ((ofGetYear() < 10)?"0":"") << ofGetYear() << ((ofGetMonth() < 10)?"0":"") << ofGetMonth() << ((ofGetDay() < 10)?"0":"") << ofGetDay() << "_" << ((ofGetHours() < 10)?"0":"") << ofGetHours() << ((ofGetMinutes() < 10)?"0":"") << ofGetMinutes() << ((ofGetSeconds() < 10)?"0":"") << ofGetSeconds();
	else {
		ofxDateTime cTime;
		timeStamp << cTime.getEpoch();
	}

	return timeStamp.str();
}

void ofxTI_HubXml::setAuthenticationCode(string authenticationCode){
	this->authenticationCode = authenticationCode;
}

void ofxTI_HubXml::setInstallationID(int installationID){
	this->installationID = installationID;
}

void ofxTI_HubXml::setSavePath(string savePath){
	this->savePath = savePath;
}

void ofxTI_HubXml::setDefaultImageExtension(string defaultImageExtension){
	if(defaultImageExtension == "")
		defaultImageExtension = DEFAULT_TI_HUB_IMAGE_EXTENSION;
	this->defaultImageExtension = defaultImageExtension;
}

void ofxTI_HubXml::setTimeStampMode(TimeStampMode tsMode){
	this->tsMode = tsMode;
}
