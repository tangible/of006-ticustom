/*
 *  ImageBar.h
 *  graffitiwall 7.5
 *
 *  Created by Eli Smiles on 11-08-10.
 *  Copyright 2011 tangibleInteraction. All rights reserved.
 *
 */
#ifndef _OFX_GUISHISHA_IMAGE_BAR
#define _OFX_GUISHISHA_IMAGE_BAR

#include "BasicBar.h"
#include "ShishaImageMap.h"

class ImageBar : public BasicBar{
protected:
	ofImage* barBackground;
	ShishaImageMap* barSlider;
	
public:
	ImageBar();
	~ImageBar();
	
	virtual void init(float x=0, float y=0, float width=DEFAULT_GUI_ELEMENT_WIDTH, float height=DEFAULT_GUI_ELEMENT_HEIGHT, int elementID=-1);
	virtual void draw(float x, float y, float w, float h, bool borders);
	
	bool loadImageMap(string bgName, string sliderName);
};

#endif
