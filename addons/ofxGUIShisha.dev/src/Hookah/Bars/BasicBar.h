/*
 *  BasicBar.h
 *  openFrameworks
 *
 *  Created by Pat Long on 09/04/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */
#ifndef _OFX_GUISHISHA_BASIC_BAR
#define _OFX_GUISHISHA_BASIC_BAR

#include "ShishaContainer.h"
#include "BasicButton.h"

#define BAR_STYLE_HORIZONTAL	0
#define BAR_STYLE_VERTICAL		1

class BasicBar : public ShishaContainer{
protected:
	class BarSlider : public BasicButton{
		protected:
			BasicBar* bar;
			string globalDragger;
		
			bool doGlobalDrag(string cursorID);
		
			virtual bool isHovered(string cursorID, float x, float y);
		
			virtual void onDrag(string cursorID);
			virtual void onPress(string cursorID);
			virtual void onRelease(string cursorID);
		
			virtual void drawAsRectangle(float x, float y, float w, float h, bool borders);
			virtual void drawAsCircle(float x, float y, float w, float h, bool borders);
			
		public:
			BarSlider(BasicBar* bar);
			~BarSlider();
	};
	
	ofRectangle barArea;
	float minValue, maxValue, tickInc;
	bool tickLock, drawTicks;
	float cValue;
	
	bool inputEnabled;
	
	BasicButton* slider;
	bool lockToSlider;
	
	virtual void positionSlider();
	
	virtual void drawAsRectangle(float x, float y, float w, float h, bool borders);
	virtual void drawAsCircle(float x, float y, float w, float h, bool borders);
	
	virtual float getTickLockedValue(float value);
	
	virtual void setValueFromInput(float x, float y);
	virtual void setValueFromInputRectangle(float x, float y);
	virtual void setValueFromInputCircle(float x, float y);
	
	virtual void sliderDragged(string cursorID, float x, float y);
	
	virtual void onDrag(string cursorID);
	virtual void onDragOn(string cursorID);
	virtual void onPress(string cursorID);
	
	virtual void onMove(float xMove, float yMove);
	
public:
	BasicBar();
	~BasicBar();
	virtual void init(float x=0, float y=0, float width=DEFAULT_GUI_ELEMENT_WIDTH, float height=DEFAULT_GUI_ELEMENT_HEIGHT, int elementID=-1);
	
	int getGuiState();
	
	BasicButton* getSlider();
	void disableSlider();
	virtual void enableSlider(bool lockToSlider=false, int sliderShape=GUI_ELEMENT_SHAPE_RECTANGLE);
	
	float getRangeSize();
	float getValue();
	float getValueAsPercent();
	
	void setCurrentValue(float cValue=0.0);
	void setCurrentValueByPercent(float valuePercent=0.0);
	void setDrawTicks(bool drawTicks=true);
	void setInputEnabled(bool inputEnabled=true);
	void setMinValue(float minValue=0.0);
	void setMaxValue(float maxValue=100.0);
	void setTickCount(int tickCount=-1.0);
	void setTickInc(float tickInc=1.0);
	void setTickLock(bool tickLock=true);
	
	
	virtual ShishaElement* setIntValue(string selector=DEFAULT_SELECTOR, int value=0);
	virtual ShishaElement* setFloatValue(string selector=DEFAULT_SELECTOR, float value=0.0);
	
	virtual void setFloat(float value=0.0);
	
	virtual ShishaElement* getElement(string selector=DEFAULT_SELECTOR, string& subSelector="");
	virtual float getFloat(string selector=DEFAULT_SELECTOR);
	virtual int getInt(string selector=DEFAULT_SELECTOR);
	
};

#endif
