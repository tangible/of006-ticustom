/*
 *  ShishaTabContainer.h
 *  openFrameworks
 *
 *  Created by Pat Long on 07/12/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */
#ifndef _OFX_GUISHISHA_SHISHATABCONTAINER
#define _OFX_GUISHISHA_SHISHATABCONTAINER

#include "ShishaContainer.h"
#include "ToggleButton.h"

#define TABCONTAINER_ANIMATE_MODE_NONE				0
#define TABCONTAINER_ANIMATE_MODE_SLIDE_TOP			1
#define TABCONTAINER_ANIMATE_MODE_SLIDE_BOTTOM		2
#define TABCONTAINER_ANIMATE_MODE_SLIDE_LEFT		3
#define TABCONTAINER_ANIMATE_MODE_SLIDE_RIGHT		4

#define TABCONTAINER_ANIMATE_MODE_SLIDE_TOPLEFT		5
#define TABCONTAINER_ANIMATE_MODE_SLIDE_TOPRIGHT	6
#define TABCONTAINER_ANIMATE_MODE_SLIDE_BOTTOMLEFT	7
#define TABCONTAINER_ANIMATE_MODE_SLIDE_BOTTOMRIGHT	8

#define DEFAULT_TABCONTAINER_ANIMATE_MODE	TABCONTAINER_ANIMATE_MODE_NONE
#define DEFAULT_TABCONTAINER_ANIMATE_SPEED	25

#define DEFAULT_TABCONTAINER_TAB_WIDTH	75
#define DEFAULT_TABCONTAINER_TAB_HEIGHT	25

class ShishaTabManager;

class ShishaTabContainer : public ShishaContainer{
protected:
	class ShishaTab : public ToggleButton{
	protected:
		ShishaTabContainer* container;
		bool moveContainer;
		
		virtual void onMove(float xMove, float yMove);
		
		virtual void onToggleOn();
		virtual void onToggleOff();
		
	public:
		ShishaTab(ShishaTabContainer* container=NULL):ToggleButton(){this->container = container; this->moveContainer = false;};
		~ShishaTab(){};
		
		void setMoveContainer(bool moveContainer=true);
	};
	
	ShishaTab* containerTab;
	ShishaTabManager* tabManager;
	bool active, animatingHide, animatingShow, doneAnimating, waitingToShow;
	int animationMode, animationSpeed;
	bool autoHideEnabled;
	int autoHideTime, showTime;
	int lastActionTime;
	ofRectangle hiddenDimensions;
	ofRectangle visibleDimensions;
	
	virtual void initContainerTab();
	virtual void animateHide();
	virtual void animateShow();
	virtual bool checkAnimationFinished();
	virtual void finishAnimating();
	
	virtual bool shouldAutoHide();
	
	virtual void onPosition(float x, float y, float oldX, float oldY);
	virtual void onResize(float width, float height);
	
public:
	ShishaTabContainer();
	~ShishaTabContainer();
	
	virtual void init(float x=0, float y=0, float width=DEFAULT_GUI_ELEMENT_WIDTH, float height=DEFAULT_GUI_ELEMENT_HEIGHT, string name="", int elementID=-1);
	virtual void update();
	
	float getVisibleHeight();
	float getVisibleWidth();
	
	ShishaElement* getContainerTab();
	ShishaElement* getTabManager();
	virtual bool getBool(string selector=DEFAULT_SELECTOR);
	virtual ShishaElement* getElement(string selector=DEFAULT_SELECTOR, string& subSelector="");
	bool isActive();
	bool isAnimating();
	bool isAnimatingHide();
	bool isAnimatingShow();
	bool isDoneAnimating();
	bool isWaitingToShow();
	
	virtual void doHide(bool immediate=false);
	virtual void doShow(bool force=false);
	virtual bool toggleActive();
	virtual void setLabel(string label);
	void setTabManager(ShishaTabManager* tabManager);
	void setMoveWithTab(bool moveWithTab=true);
	void setAnimation(int animationMode=DEFAULT_TABCONTAINER_ANIMATE_MODE, int animationSpeed=DEFAULT_TABCONTAINER_ANIMATE_SPEED);
	void setAnimationMode(int animationMode=DEFAULT_TABCONTAINER_ANIMATE_MODE);
	void setAnimationSpeed(int animationSpeed=DEFAULT_TABCONTAINER_ANIMATE_SPEED);
	void setAutoHideEnabled(bool autoHideEnabled=true);
	void setAutoHideTime(int autoHideTime=-1);
	void setHiddenDimensions(float x, float y, float w, float h);
	
	virtual bool checkCursorHover(int x, int y, string cursorID, float w=1.0, float h=1.0);
	virtual bool checkCursorDrag(int x, int y, int button, string cursorID, float w=1.0, float h=1.0);
	virtual bool checkCursorPress(int x, int y, int button, string cursorID, float w=1.0, float h=1.0);
	virtual bool checkCursorRelease(int x, int y, int button, string cursorID, float w=1.0, float h=1.0);
	
	virtual bool checkKeyPressed(int key);
	virtual bool checkKeyReleased(int key);
};

#include "ShishaTabManager.h"

#endif
