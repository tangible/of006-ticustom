/*
 *  TextOutput.cpp
 *  openFrameworks
 *
 *  Created by Pat Long on 26/05/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */

#include "TextOutput.h"

TextOutput::TextOutput(){
}

TextOutput::~TextOutput(){
	if(this->manageCustomColour && this->customColour != NULL){
		delete this->customColour;
		this->customColour = NULL;
	}
	if(this->manageCustomFont && this->customFont != NULL){
		delete this->customFont;
		this->customFont = NULL;
	}	
}

void TextOutput::init(float x, float y, float width, float height, int elementID){
	this->customColour = NULL;
	this->manageCustomColour = false;
	this->customFont = NULL;
	this->manageCustomFont = false;
	this->lineHeightMultiplier = 1.0;
	ShishaElement::init(x, y, width, height, elementID);
	this->elementType = SHISHA_TYPE_HOOKAH_TEXT_OUTPUT;
	this->setName("Text");
	this->setLabel("Text");
	this->setStringValue();
	this->setPadding();
	this->setEnableWrapping(true);
	this->setDrawStartSpaces();
	this->maxLineCount = -1;
	this->maxTextLength = -1;
}

void TextOutput::draw(float x, float y, float w, float h){
	this->draw(x, y, w, h, this->getTheme()->drawTextOutputBorders());
}

void TextOutput::draw(float x, float y, float w, float h, bool borders){	
	if(borders){
		float labelOffset = this->getTextWidth(this->getLabel());
		// only draw the background if we're also drawing borders
		this->getTheme()->setColour(SHISHA_COLOUR_BACKGROUND, this);
		ofFill();
		ofRect(x+labelOffset, y, w-labelOffset, h);
		
		this->getTheme()->setColour(SHISHA_COLOUR_BORDER, this);	
		ofNoFill();
		ofRect(x+labelOffset, y, w-labelOffset, h);
		ofFill();
	}
	
	if(this->hasCustomColour())
		this->customColour->activateColour();
	else
		this->getTheme()->setColour(SHISHA_COLOUR_FOREGROUND_1, this);
	if(this->enableWrapping)
		this->drawWrapped(x, y, w, h);
	else
		this->drawUnwrapped(x, y, w, h);
	this->getTheme()->setColour();
}

void TextOutput::drawUnwrapped(float x, float y, float w, float h){
	string output = this->getOutputValue();
	if(this->hasCustomFont() && this->customFont->bLoadedOk)
		this->customFont->drawString(output, this->padding.x+x, this->padding.y+y+(this->height+this->getTextHeight(output))/2.0-1);
	else
		this->getTheme()->drawText(output, this->padding.x+x, this->padding.y+y+(this->height+this->getTheme()->getTextHeight(output))/2.0-1, false);
}

void TextOutput::drawWrapped(float x, float y, float w, float h){
	int maxLines = this->maxLineCount;
	if(maxLines <= -1)
		maxLines = this->textLines.size();
	
	string label = this->getLabel();
	float labelOffset = (label == "")?0.0:this->getTextWidth(label);
	
	if(label != ""){
		if(this->hasCustomFont() && this->customFont->bLoadedOk)
			this->customFont->drawString(label, this->padding.x+x, this->padding.y+y+(this->height+this->getTextHeight(label))/2.0-1);
		else
			this->getTheme()->drawText(label, this->padding.x+x, this->padding.y+y+(this->height+this->getTheme()->getTextHeight(label))/2.0-1, false);
	}
	
	for(int i=0; i < this->textLines.size() && i < maxLines; i++){
		string cLine = this->getOutputLine(i);
		if(!drawStartSpaces && cLine.length() > 0 && cLine.at(0) == ' ')
			cLine = cLine.substr(1);
		
		if(this->hasCustomFont() && this->customFont->bLoadedOk)
			this->customFont->drawString(cLine, x+this->padding.x+labelOffset, y+this->padding.y+this->getTextHeight("Ay")*((float)i+0.5));
		else
			this->getTheme()->drawText(cLine, x+this->padding.x+labelOffset, y+this->padding.y+this->getTheme()->getTextHeight("Ay")*(i+1), false);
	}
}

void TextOutput::startNewLine(string lineText){
	if(this->maxLineCount != -1 && this->textLines.size() >= this->maxLineCount)
		return;
	if(lineText.length() > 0 && lineText.at(lineText.length()-1) == '\n')
		lineText = lineText.substr(0, lineText.length()-1);
	if(lineText == "" || !this->isWrappingEnabled() || this->getTextWidth(lineText) <= this->getMaxLineWidth())
		this->textLines.push_back(lineText);
	else{
		int cSpace = lineText.find(' ');
		// we have a space and at least the first word will fit on a line...
		if(cSpace != string::npos && this->getTextWidth(lineText.substr(0, cSpace)) <= this->getMaxLineWidth()){
			// we found a space... check word by word
			string cWord = lineText.substr(0, cSpace);
			int goodSpace = -1;
			while(cSpace != string::npos && this->getTextWidth(cWord) <= this->getMaxLineWidth()){
				goodSpace = cSpace;
				cSpace = lineText.find(' ', cSpace+1);
				if(cSpace != string::npos)
					cWord = lineText.substr(0, cSpace);
			}
			if(goodSpace != -1){
				this->textLines.push_back(lineText.substr(0, goodSpace+1));
				this->startNewLine(lineText.substr(goodSpace+1));
			}
		}
		else{
			// no space... must break the word, so check character by character
			int cIndex = 0;
			string cWord = "";
			bool appendPrev = false;
			if(this->textLines.size() > 0){
				// might we fit some of our text on the previous line?
				string prevLine = this->textLines[this->textLines.size()-1];
				if(this->getTextWidth(prevLine) < this->getMaxLineWidth()){
					cWord = prevLine;
					appendPrev = true;
				}
			}
			// build it up character by character until we've exceeded the line size
			while(this->getTextWidth(cWord) <= this->getMaxLineWidth())
				cWord += lineText.at(cIndex++);
			
			if(cIndex > 0){
				cIndex--; // whatever the last character we tried to add made us exceed line size, so go back one
				if(cIndex == 0){
					// we didn't fit anything on the last line... how much can we fit on this one?
					appendPrev = false;
					cWord = "";
					while(this->getTextWidth(cWord) <= this->getMaxLineWidth())
						cWord += lineText.at(cIndex++);
					cIndex--;
					
				}
				if(cIndex > 0){
					if(appendPrev)
						this->textLines[this->textLines.size()-1] += lineText.substr(0, cIndex);
					else
						this->textLines.push_back(lineText.substr(0, cIndex));
					
					// put the rest onto a new line
					this->startNewLine(lineText.substr(cIndex));
				}
			}
		}
	}
}

void TextOutput::wrapLines(){
	if(!this->isWrappingEnabled())
		return;
	this->textLines.clear();
	if(this->textValue.find('\n') != string::npos){
		vector<string> splits = split(this->textValue, '\n');
		string cLine = "";
		for(int i = 0; i < splits.size(); i++){
			cLine = splits[i];
			if(i < splits.size()-1 || this->textValue.at(this->textValue.length()-1) == '\n')
				cLine += '\n';
			
			this->startNewLine(cLine);
		}
	}
	else
		this->startNewLine(this->textValue);
}

bool TextOutput::isWrappingEnabled(){
	return this->enableWrapping;
}

int TextOutput::getMaxLineCount(){
	return this->maxLineCount;
}

int TextOutput::getMaxTextLength(){
	return this->maxTextLength;
}

bool TextOutput::hasCustomColour(){
	return (this->customColour != NULL);
}

bool TextOutput::hasCustomFont(){
	return (this->customFont != NULL);
}

void TextOutput::setCustomColour(float r, float g, float b, float a){
	this->setCustomColour(new ColourRGBA(r, g, b, a), true);
}

void TextOutput::setCustomColour(ColourRGBA* customColour, bool manageCustomColour){
	this->customColour = customColour;
	this->manageCustomColour = manageCustomColour;
}

void TextOutput::setCustomFont(string fontName, int fontSize){
	ofTrueTypeFont* newFont = new ofTrueTypeFont();
	newFont->loadFont(fontName, fontSize);
	if(!newFont->bLoadedOk){
		delete newFont;
		newFont = NULL;
		return;
	}
	
	if(newFont != NULL && newFont->bLoadedOk){
		if(this->customFont != NULL){
			if(this->manageCustomFont)
				delete this->customFont;
			this->customFont = NULL;
		}
		this->setCustomFont(newFont, true);
	}
}

void TextOutput::setCustomFont(ofTrueTypeFont* customFont, bool manageCustomFont){
	this->customFont = customFont;
	this->manageCustomFont = manageCustomFont;
}

void TextOutput::setDrawStartSpaces(bool drawStartSpaces){
	this->drawStartSpaces = drawStartSpaces;
}

int TextOutput::getMaxLineWidth(){
	return (this->getWidth()-this->getHPadding()-this->getTextWidth(this->getLabel()));
}

int TextOutput::getTextWidth(string text){
	if(this->hasCustomFont()){
		int result = 0;
		if(this->customFont->bLoadedOk){
			// we gotta do some funky stuff if it starts or ends with a space... stringWidth doesn't calculate properly otherwise
			string tempChecker = text;
			bool spaceEnder = false;
			bool spaceStarter = false;
			if(tempChecker != "" && tempChecker.at(0) == ' '){
				tempChecker = '.' + tempChecker;
				spaceStarter = true;
			}
			if(tempChecker != "" && tempChecker.at(tempChecker.length()-1) == ' '){
				tempChecker += '.';
				spaceEnder = true;
			}
			result = this->customFont->stringWidth(tempChecker);
			if(spaceStarter)
				result -= (this->customFont->stringWidth(".") - 1);
			if(spaceEnder)
				result -= (this->customFont->stringWidth(".") - 1);
		}
		else
			result = text.length() * 8.2;
		return result;
	}
	return this->getTheme()->getTextWidth(text);
}

int TextOutput::getTextHeight(string text){
	if(text == "")
		text = "AYy";
	if(this->hasCustomFont()){
		if(this->customFont->bLoadedOk)
			return this->customFont->stringHeight(text)*this->lineHeightMultiplier;
	}
	return this->getTheme()->getTextHeight(text);
}

string TextOutput::getOutputValue(){
	stringstream output;
	if(this->getLabel() != "")
		output << this->getLabel();
	output << this->textValue;
	return output.str();
}

string TextOutput::getOutputLine(int i){
	if(i >= 0 && i < this->textLines.size())
		return this->textLines[i];
	return "";
}

float TextOutput::getHPadding(){
	return (this->getPaddingL()+this->getPaddingR());
}

float TextOutput::getVPadding(){
	return (this->getPaddingT()+this->getPaddingB());
}

float TextOutput::getPaddingL(){
	return this->padding.x;
}

float TextOutput::getPaddingT(){
	return this->padding.y;
}

float TextOutput::getPaddingR(){
	return this->padding.width;
}

float TextOutput::getPaddingB(){
	return this->padding.height;
}

void TextOutput::setEnableWrapping(bool enableWrapping){
	this->enableWrapping = enableWrapping;
}

void TextOutput::setLineHeightMultiplier(float lineHeightMultiplier){
	this->lineHeightMultiplier = lineHeightMultiplier;
}

void TextOutput::setMaxLineCount(int maxLineCount){
	this->maxLineCount = maxLineCount;
	if(maxLineCount != -1){
		if(maxLineCount == 0)
			this->setMaxTextLength(0);
		else if(maxLineCount > 1){
			int charsPerLine = 0;
			stringstream builder;
			while(this->getTextWidth(builder.str()) < this->getMaxLineWidth())
				builder << "W"; // widest character
			charsPerLine = builder.str().length();
			builder.str("");
			this->setMaxTextLength(maxLineCount*charsPerLine);
		}
	}
}

void TextOutput::setMaxTextLength(int maxTextLength){
	this->maxTextLength = maxTextLength;
}

void TextOutput::setPadding(float l, float t, float r, float b){
	this->padding.x = l;
	this->padding.y = t;
	this->padding.width = r;
	this->padding.height = b;
}

string TextOutput::getString(string selector){
	if(this->selectSelf(selector))
		return this->textValue;
	return ShishaElement::getString(selector);
}

void TextOutput::setString(string value){
	this->textValue = value;
	this->wrapLines();
}
