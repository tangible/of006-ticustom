/*
 *  TextOutput.h
 *  openFrameworks
 *
 *  Created by Pat Long on 26/05/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */
#ifndef _OFX_GUISHISHA_TEXT_OUTPUT
#define _OFX_GUISHISHA_TEXT_OUTPUT

#include "ShishaElement.h"

class TextOutput : public ShishaElement{
	protected:
		ofRectangle padding;
		string textValue;
		vector<string> textLines;
	
		bool drawStartSpaces;
	
		ofTrueTypeFont* customFont;
		bool manageCustomFont;
	
		ColourRGBA* customColour;
		bool manageCustomColour;

		bool enableWrapping;
		int maxLineCount, maxTextLength;
	
		float lineHeightMultiplier;
	
		void startNewLine(string lineText="");
		virtual void wrapLines();
	
		bool hasCustomColour();
		bool hasCustomFont();
	
		int getMaxLineWidth();
		int getTextWidth(string text);
		int getTextHeight(string text="");
		virtual string getOutputValue();
		virtual string getOutputLine(int i);
	
		virtual void drawUnwrapped(float x, float y, float w, float h);
		virtual void drawWrapped(float x, float y, float w, float h);
		
	public:
		TextOutput();
		~TextOutput();

		virtual void init(float x=0, float y=0, float width=DEFAULT_GUI_ELEMENT_WIDTH, float height=DEFAULT_GUI_ELEMENT_HEIGHT, int elementID=-1);
	
		virtual void draw(float x, float y, float w, float h);
		virtual void draw(float x, float y, float w, float h, bool borders);

		bool isWrappingEnabled();
		int getMaxLineCount();
		int getMaxTextLength();
		float getHPadding();
		float getVPadding();
		float getPaddingL();
		float getPaddingT();
		float getPaddingR();
		float getPaddingB();
	
		virtual void setCustomColour(float r, float g, float b, float a);
		virtual void setCustomColour(ColourRGBA* customColour=NULL, bool manageCustomColour=true);
		virtual void setCustomFont(string fontName, int fontSize);
		virtual void setCustomFont(ofTrueTypeFont* customFont=NULL, bool manageCustomFont=true);
		virtual void setDrawStartSpaces(bool drawStartSpaces=true);
		virtual void setEnableWrapping(bool enableWrapping=true);
		virtual void setLineHeightMultiplier(float lineHeightMultiplier=1.0);
		virtual void setMaxLineCount(int maxLineCount=-1);
		virtual void setMaxTextLength(int maxTextLength=-1);
		virtual void setPadding(float l=0.0, float t=0.0, float r=0.0, float b=0.0);
	
		virtual string getString(string selector=DEFAULT_SELECTOR);
		virtual void setString(string value="");
};

#endif
