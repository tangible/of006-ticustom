/*
 *  ofxSlideshowFrame.h
 *  openFrameworks
 *
 *  Created by Eli Smiles on 11-08-05.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */
#ifndef _OFX_SLIDESHOW_FRAME
#define _OFX_SLIDESHOW_FRAME

#include "ofMain.h"
#include "ofxThreadedImageSaver.h"

class ofxSlideshow;

class ofxSlideshowFrame{
	protected:
		ofxSlideshow* slideshow;
		ofxThreadedImageSaver imageSaver;
	
		ofImage* image;
		string imageName;
	
		int index;
	
		bool changed;
	
		bool saveImage(string saveAs);
		
	public:
		ofxSlideshowFrame(string imageName="");
		~ofxSlideshowFrame();
	
		void draw();
	
		ofImage* getImage();
		string getImageName();
		int getIndex();
		string getIndexAsString();
	
		bool hasChanged();
	
		bool canLoad();
		bool isLoaded();
	
		bool load();
		void unload();
	
		bool renameImage(string newName);
		string saveImage();
	
		void setFromPixels(unsigned char* pixels, int w, int h, int imageType=OF_IMAGE_COLOR);
		bool setImageName(string imageName="");
		void setIndex(int index=-1);
		void setSlideshow(ofxSlideshow* slideshow=NULL);
};

#include "ofxSlideshow.h"

#endif
