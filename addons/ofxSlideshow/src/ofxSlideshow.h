/*
 *  ofxSlideshow.h
 *  openFrameworks
 *
 *  Created by Eli Smiles on 11-08-03.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */
#ifndef _OFX_SLIDESHOW
#define _OFX_SLIDESHOW

#include "ofMain.h"
#include "ofxDirList.h"
#include "ofxTI_Utils.h"
#include "ofxSlideshowFrame.h"
#include "ofxSlideshowSaver.h"

#define SLIDESHOW_DEFAULT_BUFFER_SIZE	5
#define SLIDESHOW_DEFAULT_FRAME_DELAY	1000
#define SLIDESHOW_DEFAULT_IMAGE_TYPE	"png"

typedef enum {SLIDESHOW_FORWARD, SLIDESHOW_REVERSE} SlideshowDirection;
typedef enum {SLIDESHOW_PLAYING, SLIDESHOW_PAUSED, SLIDESHOW_STOPPED} SlideshowState;

class ofxSlideshow{
protected:
	vector<ofxSlideshowFrame*> frames;
	
	string baseSavePath;
	
	int activeFrameIndex;
	int bufferSize;
	SlideshowDirection direction;
	int frameDelay;
	string imageType;
	int lastSwitch;
	int loadedThisFrame;
	bool loopingEnabled;
	int maxLoadPerFrame;
	SlideshowState state;
	
	float maxWidth, maxHeight;
	
	ofxSlideshowSaver* movieSaver;
	
	void clearFrames();
	
	int getBufferStartIndex();
	int getBufferEndIndex();
	
	bool shouldBuffer(int frameIndex);
	void updateFrame(int frameIndex);
	
public:
	ofxSlideshow();
	~ofxSlideshow();
	
	virtual void update();
	
	virtual void outputBuffer();
	
	int addFrame(string imageName="", int index=-1);
	
	int loadSlideshow(string directory);
	void resetSlideshow();
	bool saveSlideshow(string movieName, float* backgroundColour=NULL, ofImage* backgroundImage=NULL, ofImage* overlayImage=NULL);
	
	bool isSaving();
	float getSavePercent();
	
	int getActiveFrameIndex();
	string getBaseSavePath();
	int getBufferSize();
	ofxSlideshowFrame* getFrame(int index=-1);
	int getFrameCount();
	bool getLoopingEnabled();
	string getNewImageName(ofxSlideshowFrame* forFrame=NULL, bool forceUnique=true);
	float getScaledFrameDimensions(float& width, float& height);
	
	bool isPlaying();
	bool isPaused();
	bool isStopped();
	
	void play();
	void pause();
	void stop();
	
	int nextFrame();
	int prevFrame();
	int setActiveFrame(int frameIndex, bool forceLoad=false);
	
	void setBaseSavePath(string baseSavePath="");
	void setBufferSize(int bufferSize=SLIDESHOW_DEFAULT_BUFFER_SIZE);
	void setDirection(SlideshowDirection direction=SLIDESHOW_FORWARD);
	void setFrameDelay(int frameDelay=SLIDESHOW_DEFAULT_FRAME_DELAY);
	void setImageType(string imageType=SLIDESHOW_DEFAULT_IMAGE_TYPE);
	void setLoopingEnabled(bool loopingEnabled=true);
	void setMaxFrameDimensions(float maxWidth=-1.0, float maxHeight=-1.0);
	void setMaxLoadPerFrame(int maxLoadPerFrame=1);
};

#endif
