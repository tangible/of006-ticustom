/*
 *  ofxSlideshowSaver.cpp
 *  graffitiwall 7.5
 *
 *  Created by Eli Smiles on 11-08-17.
 *  Copyright 2011 tangibleInteraction. All rights reserved.
 *
 */

#include "ofxSlideshowSaver.h"

ofxSlideshowSaver::ofxSlideshowSaver(ofxSlideshow* slideshow){
	this->slideshow = slideshow;
	savePercent = 0.0;
	saveComplete = false;
	backgroundColour = NULL;
	backgroundImage = NULL;
	overlayImage = NULL;
}

ofxSlideshowSaver::~ofxSlideshowSaver(){
	if(this->backgroundImage != NULL){
		delete this->backgroundImage;
		this->backgroundImage = NULL;
	}
	
	if(this->overlayImage != NULL){
		delete this->overlayImage;
		this->overlayImage = NULL;
	}
}

void ofxSlideshowSaver::threadedFunction(){	
	saveComplete = false;
	savePercent = 0.0;
	if(slideshow != NULL){
		ofImage* cImage = NULL;
		ofxSlideshowFrame* cFrame = NULL;
		float percentPerFrame = 100.0 / (float)slideshow->getFrameCount();
		
		bool saverSetup = false;
		float w, h;
		
		for(int i=0; i < slideshow->getFrameCount(); i++){
			cFrame = slideshow->getFrame(i);
			if(cFrame != NULL){
				string frameName = cFrame->getImageName();
				if(frameName != ""){
					cImage = new ofImage();
					cImage->setUseTexture(false);
					
					if(cImage->loadImage(frameName)){
						if(!saverSetup){
							w = cImage->getWidth();
							h = cImage->getHeight();
							movieSaver.setup(w, h, movieName);
							saverSetup = true;
						}
						unsigned char* pixels = getRenderedPixels(cImage, w, h);
						if(pixels != NULL){
							movieSaver.addFrame(pixels, this->frameLength);
							delete [] pixels;
							pixels = NULL;
						}
					}
					
					delete cImage;
					cImage = NULL;
				}
			}
			savePercent += percentPerFrame;
		}
		
		movieSaver.finishMovie();
	}
	else{
		savePercent = 100.0;
	}
	
	saveComplete = true;
	stopThread();
}

unsigned char* ofxSlideshowSaver::getRenderedPixels(ofImage* forFrame, int w, int h){
	unsigned char* pixels = NULL;
	if(forFrame != NULL){
		if(forFrame->getWidth() != w || forFrame->getHeight() != h)
			forFrame->resize(w, h);
		unsigned char* framePixels = forFrame->getPixels();
		int frameBPP = forFrame->bpp / 8;
		
		unsigned char* backgroundPixels = NULL; 
		int backgroundBPP = 0;
		if(backgroundImage != NULL){
			if(backgroundImage->getWidth() != w || backgroundImage->getHeight() != h)
				backgroundImage->resize(w, h);
			backgroundPixels = backgroundImage->getPixels();
			backgroundBPP = backgroundImage->bpp / 8;
		}
		
		unsigned char* overlayPixels = NULL;
		int overlayBPP = 0;
		if(overlayImage != NULL){
			if(overlayImage->getWidth() != w || overlayImage->getHeight() != h)
				overlayImage->resize(w, h);
			overlayPixels = overlayImage->getPixels();
			overlayBPP = overlayImage->bpp / 8;
		}
		
		pixels = new unsigned char[w*h*3];
		for(int i=0; i < w; i++){
			for(int j=0; j < h; j++){
				int renderIndex = (j*w+i)*3;
				int frameIndex = (j*w+i)*frameBPP;
				int backgroundIndex = (j*w+i)*backgroundBPP;
				int overlayIndex = (j*w+i)*overlayBPP;
				
				for(int b=0; b < 3; b++){
					pixels[renderIndex+b] = (backgroundColour != NULL)?(int)(backgroundColour[b]*255.0):0;
					
					if(backgroundPixels != NULL){
						float bgBlend = (float)((backgroundBPP > 3)?backgroundPixels[backgroundIndex+3]:255)/255.0;
						int bgb = (backgroundBPP > 1)?b:0;
						pixels[renderIndex+b] = (int)( (float)pixels[renderIndex+b]*(1.0-bgBlend) + (float)backgroundPixels[backgroundIndex+bgb]*bgBlend );
					}
					
					float fBlend = (float)((frameBPP > 3)?framePixels[frameIndex+3]:255)/255.0;
					int fb = (frameBPP > 1)?b:0;
					pixels[renderIndex+b] = (int)( (float)pixels[renderIndex+b]*(1.0-fBlend) + (float)framePixels[frameIndex+fb]*fBlend );
					
					if(overlayPixels != NULL){
						float olBlend = (float)((overlayBPP > 3)?overlayPixels[overlayIndex+3]:255)/255.0;
						int olb = (overlayBPP > 1)?b:0;
						pixels[renderIndex+b] = (int)( (float)pixels[renderIndex+b]*(1.0-olBlend) + (float)overlayPixels[overlayIndex+olb]*olBlend );
					}
				}
			}
		}
	}
	return pixels;
}

float ofxSlideshowSaver::getSavePercent(){
	return savePercent;
}

bool ofxSlideshowSaver::isFinished(){
	return saveComplete; //(savePercent >= 100.0);
}

string ofxSlideshowSaver::saveSlideshow(string movieName){
	this->movieName = movieName;
	startThread(true, false);
	return movieName;
}

void ofxSlideshowSaver::setBackgroundColour(float* backgroundColour){
	this->backgroundColour = backgroundColour;
}

void ofxSlideshowSaver::setBackgroundImage(ofImage* backgroundImage){
	if(this->backgroundImage != NULL){
		delete this->backgroundImage;
		this->backgroundImage = NULL;
	}
	if(backgroundImage != NULL){
		this->backgroundImage = new ofImage();
		this->backgroundImage->setUseTexture(false);
		this->backgroundImage->setFromPixels(backgroundImage->getPixels(), backgroundImage->getWidth(), backgroundImage->getHeight(), backgroundImage->type);
	}
//	this->backgroundImage = backgroundImage;
}

void ofxSlideshowSaver::setFrameLength(float frameLength){
	this->frameLength = frameLength;
}

void ofxSlideshowSaver::setOverlayImage(ofImage* overlayImage){
	if(this->overlayImage != NULL){
		delete this->overlayImage;
		this->overlayImage = NULL;
	}
	if(overlayImage != NULL){
		this->overlayImage = new ofImage();
		this->overlayImage->setUseTexture(false);
		this->overlayImage->setFromPixels(overlayImage->getPixels(), overlayImage->getWidth(), overlayImage->getHeight(), overlayImage->type);
	}	
//	this->overlayImage = overlayImage;
}
