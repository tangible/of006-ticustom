/*
 *  Image2WebHttpService.cpp
 *  openFrameworks
 *
 *  Created by Pat Long on 26/02/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#include "Image2WebHttpService.h"

Image2WebHttpService::Image2WebHttpService(){
	ofxHttpUtil.setTimeoutSeconds(15);
	ofxHttpEvents.addListener(this);
	this->formReady = false;
}

Image2WebHttpService::~Image2WebHttpService(){
}

bool Image2WebHttpService::isFormReady(){
	return this->formReady;
}

bool Image2WebHttpService::postImage(ofImage image, map<string,string> params){
	if(this->setParameters(params)){
		ofxHttpForm apiForm;
		apiForm.name = this->getFormName();
		apiForm.action = this->getFormAction();

		if(image.getFileName() != ""){
			if(this->addParams(apiForm, ofToDataPath(image.getFileName(), true), params) > 0)
				return (this->postImageForm(apiForm) != -1);
		}
		else{
			stringstream messageBuilder;
			messageBuilder << "Image2WebHttpService: No filename was given for post image request." << endl;
			ofLog(OF_LOG_ERROR, messageBuilder.str());
			messageBuilder.str("");
		}
	}
	
	return false;
}

int Image2WebHttpService::postImageForm(ofxHttpForm& apiForm, bool multipart, int method){
	int result = 0;
	stringstream messageBuilder;
	
	if(apiForm.action != ""){
		if(this->isFormReady()){
			apiForm.method = method;
			apiForm.multipart = multipart;
			
			this->setResponseCode(I2W_RESPONSE_WAITING);
			ofxHttpUtil.addForm(apiForm);
		}
		else{
			messageBuilder << "Image2WebHttpService: Form is not ready." << endl;
			ofLog(OF_LOG_ERROR, messageBuilder.str());
			messageBuilder.str("");
			result = -1;
		}
	}
	else{
		messageBuilder << "Image2WebHttpService: No form action was given for image upload request." << endl;
		ofLog(OF_LOG_ERROR, messageBuilder.str());
		messageBuilder.str("");
		result = -1;
	}
	
	if(result == -1)
		this->setResponseCode(I2W_RESPONSE_SEND_ERROR);
	
	return result;
}

void Image2WebHttpService::newResponse( ofxHttpResponse & response ){
/**
	int status; 				// return code for the response ie: 200 = OK
	string reasonForStatus;		// text explaining the status
	string responseBody;		// the actual response
	string contentType;			// the mime type of the response
	ofTimestamp timestamp;		// time of the response
	string url;
	
	cout << "Image2WebHttpService:response:" << response.status << " - " << response.reasonForStatus << ":" << endl;
	cout << "\turl:" << response.url << ":" << endl;
	cout << "\ttype:" << response.contentType << ":" << endl;
	cout << "\tbody:" << response.responseBody << ":" << endl;*/

//	cout << "\ttime:" << response.timestamp << ":" << endl;
	if(response.status == 200)
		this->setResponseCode(I2W_RESPONSE_SUCCESS);
	else
		this->setResponseCode(I2W_RESPONSE_RESP_ERROR);
}

void Image2WebHttpService::newError(string & error){
	stringstream messageBuilder;
	messageBuilder << "Image2WebHttpService:" << error;
	ofLog(OF_LOG_ERROR, messageBuilder.str());
	messageBuilder.str("");
	this->setResponseCode(I2W_RESPONSE_RESP_ERROR);
}
