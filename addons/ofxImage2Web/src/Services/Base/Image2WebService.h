/*
 *  Image2WebService.h
 *  openFrameworks
 *
 *  Created by Pat Long on 26/02/10.
 *  Copyright 2010 Tangible Interaction Inc. All rights reserved.
 *
 */
#ifndef _OFX_IMAGE2WEB_SERVICE
#define _OFX_IMAGE2WEB_SERVICE

#include "ofMain.h"

#define DEFAULT_I2W_TEMP_IMAGE_NAME "i2w_temp_image.png"

typedef enum I2WResponseCodes{
	I2W_RESPONSE_NULL,
	I2W_RESPONSE_WAITING,
	I2W_RESPONSE_SUCCESS,
	I2W_RESPONSE_SEND_ERROR,
	I2W_RESPONSE_RESP_ERROR
} I2WResponseCode;

class Image2WebService{
	protected:
		int cResponseCode;
		vector<string> requiredParams;
	
		virtual bool checkParams(map<string,string> params){
			bool result = true;
			stringstream messageBuilder;
			for(int i=0; i < this->requiredParams.size(); i++){
				if(params.find(this->requiredParams[i]) == params.end()){
					messageBuilder << "Image2WebService: missing required '" << this->requiredParams[i] << "' parameter";
					ofLog(OF_LOG_ERROR, messageBuilder.str());
					messageBuilder.str("");
					result = false;
				}
			}
			return result;
		};
	
		virtual void setResponseCode(int _responseCode=I2W_RESPONSE_NULL){ this->cResponseCode = _responseCode; };
	
	public:
		Image2WebService(){ this->setResponseCode(); };
		~Image2WebService(){};
	
		virtual void update(){};
	
		virtual bool postImage(ofImage image, map<string,string> params)=0;
		virtual int getResponseCode(){ return this->cResponseCode; };

};

#endif
