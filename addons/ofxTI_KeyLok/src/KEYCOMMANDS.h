/*
 *  KEYCOMMANDS.h
 *
 *  Created by Pat Long (plong0) on 08/04/09.
 *  Copyright 2009 Tangible Interaction Inc. All rights reserved.
 *
 */
#ifndef KEYCOMMANDS_H_
#define KEYCOMMANDS_H_

/* COMMAND CODES */
#define TERMINATE      -1
#define KLCHECK         1
#define READAUTH        2
#define GETSN           3
#define GETVARWORD      4
#define WRITEAUTH       5
#define WRITEVARWORD    6
#define DECREMENTMEM    7
#define GETEXPDATE      8
#define CKLEASEDATE     9
#define SETEXPDATE     10
#define REMOTEUPDUPT1  13
#define REMOTEUPDUPT2  14
#define REMOTEUPDUPT3  15
#define REMOTEUPDCPT1  16
#define REMOTEUPDCPT2  17
#define REMOTEUPDCPT3  18

#endif
