/*
 *  KeyLokThreadedValidator.h
 *  emptyExample
 *
 *  Created by Eli Smiles on 25/09/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */
#ifndef KEYLOK_DISABLE_THREADS
#ifndef _TI_KEYLOK_THREADED_VALIDATOR
#define _TI_KEYLOK_THREADED_VALIDATOR

#include "KeyLok.h"
#include "ofxThread.h"

#define KEYLOK_AVERAGE_CHECK_TIME			10000 // used in setSafeToRemoveSpan

#define KEYLOK_DEFAULT_SAFE_TO_REMOVE_SPAN	100000	// default allow 10 seconds to remove dongle

class KeyLokThreadedValidator : public ofxThread {
protected:
	KeyLok* keyLok;
	KeyLokValidationMode validationMode;
	bool timedCheck;
	bool valid;
	bool safeToRemove;
	
	int lastCheck, checkDelay;
	
public:
	KeyLokThreadedValidator(KeyLok* keyLok);
	~KeyLokThreadedValidator();
	
	void start(bool timedCheck=true);
	void stop();
	
	virtual void threadedFunction();
	
	int getTimeUntilCheck(int cTime=-1);
	
	bool doCheckNow();
	bool isSafeToRemove();
	bool isValid();
	
	void setCheckDelay(int checkDelay);
	void setSafeToRemoveSpan(int safeToRemoveSpan=KEYLOK_DEFAULT_SAFE_TO_REMOVE_SPAN);
	void setValidationMode(KeyLokValidationMode validationMode=VALIDATE_FULL);
};

#endif
#endif