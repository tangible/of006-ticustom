/*
 *  ofFBOTexture.h
 *  openFrameworks
 *
 *  Created by Zach Gage on 3/28/08.
 *  Copyright 2008 STFJ.NET. All rights reserved.
 *
 */

#ifndef _FBO_TEX
#define _FBO_TEX

#include "ofMain.h"
#include <iostream>

extern float bgColor[];

class ofFBOTexture : public ofTexture
{

	public:
		~ofFBOTexture();

		void allocate(int w, int h, bool autoC, bool transparentBG=false);

		void swapIn();
		void swapOut();

		void setupScreenForMe();
		void setupScreenForThem();
	
		bool isSwappedIn();
	
		unsigned char* getPixels(unsigned char* pixels, int x=0, int y=0, int w=0, int h=0);

		void clear();
		void clear(int x, int y, int w, int h);
		void clean();
	
		void setClearColor(int r, int g, int b, int a=1);
		void setInvertY(bool invertY=true);


	protected:
		ofColor clearColor;

		GLuint fbo;					// Our handle to the FBO
		GLuint depthBuffer;			// Our handle to the depth render buffer
		bool invertY;
		bool autoClear;
		bool transparentBG;
		bool swappedIn;
};

#endif

